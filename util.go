package main

import (
	"net/http"
	"net/url"
	"os"

	"github.com/apex/log"
	"github.com/apex/log/handlers/logfmt"
	"github.com/apex/log/handlers/text"
	"github.com/mattn/go-isatty"

	"gitlab.apertis.org/infrastructure/authz-mediator/config"
)

func updateFiltered(fields map[string]interface{}, values url.Values) {
	for k, v := range values {
		if k == "code" || k == "nonce" || k == "client_secret" {
			fields[k] = "******"
		} else {
			fields[k] = v[0]
		}
	}
}

func handleFunc(pattern string, handler http.HandlerFunc) {
	http.HandleFunc(pattern, func(w http.ResponseWriter, r *http.Request) {
		fields := log.Fields{}
		err := r.ParseForm()
		if err != nil {
			log.WithError(err).Error(r.Method + " " + r.URL.Path)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		updateFiltered(fields, r.Form)
		log.WithFields(fields).Info(r.Method + " " + r.URL.Path)
		handler(w, r)
	})
}

func setupLog() {
	if isatty.IsTerminal(os.Stdout.Fd()) {
		log.SetHandler(text.New(os.Stderr))
	} else {
		log.SetHandler(logfmt.New(os.Stdout))
	}

	if debug, ok := os.LookupEnv("AUTHZ_MEDIATOR_DEBUG"); ok {
		if config.Truthy(debug) {
			log.SetLevel(log.DebugLevel)
		}
	}
}
