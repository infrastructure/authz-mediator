package lava_test

import (
	"testing"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

func TestLAVA(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "LAVA Suite")
}
