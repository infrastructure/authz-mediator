// Copyright (C) 2023, Andrej Shadura, Emanuele Aina
// Copyright (C) 2023, Collabora Limited
//
// SPDX-License-Identifier: Apache-2.0

package lava_test

import (
	"fmt"
	"sort"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"gitlab.apertis.org/infrastructure/authz-mediator/claims"
	"gitlab.apertis.org/infrastructure/authz-mediator/config"
	"gitlab.apertis.org/infrastructure/authz-mediator/lava"
)

const (
	existingEmail = "correct@apert.is"
	existingUser  = "apertis"
)

type fakeLAVAer struct {
	userGroups []string
}

func (lavaer *fakeLAVAer) GetGroups(username string) ([]string, error) {
	GinkgoWriter.Println("get groups for", username)
	if username != existingUser {
		return nil, fmt.Errorf("no user: %s", username)
	}
	return lavaer.userGroups, nil
}

func (lavaer *fakeLAVAer) SetGroups(username string, groups []string) error {
	GinkgoWriter.Println("add", username, "to groups", groups)
	if username != existingUser {
		return fmt.Errorf("no user: %s", username)
	}
	lavaer.userGroups = groups
	return nil
}

// The two functions below are not used in the LAVA backend test
func (lavaer *fakeLAVAer) GetAllUsers() ([]string, error) {
	return nil, fmt.Errorf("Not implemented")
}

func (lavaer *fakeLAVAer) SetUserActive(user string, active bool) error {
	return fmt.Errorf("Not implemented")
}

var _ = Describe("LAVA", func() {
	var (
		l     *lava.LAVA
		lavaer *fakeLAVAer
		err   error
		conf  config.Config
	)

	BeforeEach(func() {
		lavaer = &fakeLAVAer{
			userGroups: []string{
				"unmanagedgroup",
			},
		}
		l = &lava.LAVA{lavaer}
		conf = config.Config{
			Retries: 1,
			BackOff: 1,
			IncludedGroups: []string{
				"group*",
			},
		}
	})

	Describe("Apply", func() {
		Context("when claims are nil", func() {
			It("should fail and return an error", func() {
				err = l.Apply(nil, &config.Config{})
				Expect(err).To(HaveOccurred())
				Expect(err.Error()).To(Equal("no claims to apply"))
			})
		})

		Context("when there is no user for given email", func() {
			It("should fail and return an error", func() {
				err = l.Apply(&claims.Claims{Email: ""}, &config.Config{Retries: 1})
				Expect(err).To(HaveOccurred())
				Expect(err.Error()).To(ContainSubstring("no user"))
			})
		})

		Context("when claims have group lists for a new user", func() {
			claimGroups1 := []string{
				"group1", "group2", "excludedgroup",
			}
			userGroups1 := []string{
				"group1", "group2", "unmanagedgroup",
			}
			claimGroups2 := []string{
				"group2", "group3",
			}
			userGroups2 := []string{
				"group2", "group3", "unmanagedgroup",
			}

			It("adds the user to existing groups, then when new claim is applied, remove the user from the old ones", func() {
				By("Processing the first groups claim")
				err = l.Apply(&claims.Claims{
					Email:  existingEmail,
					Username: existingUser + "@example.org",
					Groups: claimGroups1,
				}, &conf)
				Expect(err).ToNot(HaveOccurred())

				sort.Strings(lavaer.userGroups)

				Expect(lavaer.userGroups).To(Equal(userGroups1))

				By("Processing the second groups claim")
				err = l.Apply(&claims.Claims{
					Email:  existingEmail,
					Username: existingUser,
					Groups: claimGroups2,
				}, &conf)
				Expect(err).ToNot(HaveOccurred())

				sort.Strings(lavaer.userGroups)

				Expect(lavaer.userGroups).To(Equal(userGroups2))
			})

		})

	})

})
