// Copyright (C) 2021, Paweł Wieczorek
// Copyright (C) 2021, Collabora Limited
//
// SPDX-License-Identifier: Apache-2.0

package lava

import (
	"fmt"

	"github.com/apex/log"
	mapset "github.com/deckarep/golang-set/v2"

	. "gitlab.apertis.org/infrastructure/authz-mediator/claims"
	"gitlab.apertis.org/infrastructure/authz-mediator/config"
)

// LAVA handles service interaction logic.
// It provides only high level functions, handles possible errors and verifies which ones can be safely ignored.
// This structure contains no data to access LAVA instance nor methods to call its API directly.
type LAVA struct {
	LAVAer
}

func NewLAVA(secret, url string) (*LAVA, error) {
	lc, err := newLavaClient(secret, url)
	if err != nil {
		return nil, fmt.Errorf("failed to create LAVA backend: %w", err)
	}
	return &LAVA{lc}, nil
}

func (l *LAVA) Apply(claims *Claims, config *config.Config) error {
	logctx := log.WithFields(log.Fields{
		"backend": "lava",
	})
	if claims == nil {
		return fmt.Errorf("no claims to apply")
	}

	logctx = logctx.WithFields(log.Fields{
		"claim_username": claims.Username,
		"claim_group":    claims.Groups,
	})

	localPart := UsernameLocalPart(claims.Username)
	logctx = logctx.WithFields(log.Fields{
		"user":           localPart,
	})

	currentGroups, err := l.GetGroups(localPart)
	if err != nil {
		return fmt.Errorf("failed to retrieve current user groups: %w", err)
	}

	currentGroupsSet := mapset.NewSet[string](currentGroups...)
	managedGroups := FilterGroups(currentGroups, config.IncludedGroups, []string{""})
	managedGroupsSet := mapset.NewSet[string](managedGroups...)
	unmanagedGroupsSet := currentGroupsSet.Difference(managedGroupsSet)

	newManagedGroups := FilterGroups(claims.Groups, config.IncludedGroups, []string{""})
	newManagedGroupsSet := mapset.NewSet[string](newManagedGroups...)

	newGroupsSet := newManagedGroupsSet.Union(unmanagedGroupsSet)

	to_add := newManagedGroupsSet.Difference(managedGroupsSet)
	to_remove := managedGroupsSet.Difference(newManagedGroupsSet)
	logctx.WithFields(log.Fields{
		"current_groups": currentGroupsSet,
		"to_add":		  to_add,
		"to_remove":	  to_remove,
	}).Info("group membership")
	err = l.SetGroups(localPart, newGroupsSet.ToSlice())

	if err != nil {
		return fmt.Errorf("failed to synchronise groups: %w", err)
	}
	return nil
}

func (l *LAVA) LockUser(user string) error {
	return l.SetUserActive(user, false)
}
