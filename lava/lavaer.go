// Copyright (C) 2021, Paweł Wieczorek
// Copyright (C) 2021, Collabora Limited
//
// SPDX-License-Identifier: Apache-2.0

package lava

import (
	b64 "encoding/base64"
	"fmt"
	"net/url"
	"strings"

	"alexejk.io/go-xmlrpc"
)

// LAVAer provides passthrough interface to the LAVA API.
// It does not handle any errors. It only ensures passed arguments comply with LAVA API requirements.
//
// Mocking this interface allows testing higher level logic without LAVA instance.
type LAVAer interface {
	// Get the groups a user is a member of (user is a username)
	GetGroups(user string) ([]string, error)
	// Set the groups a user is a member of (user is a username)
	SetGroups(user string, groups []string) error
	// Get all users registered on this LAVA instance
	GetAllUsers() ([]string, error)
	// Toggle whether user is active (user is a username, active=false locks)
	//
	// Note that this interface has significant caveats: because
	// of the lifespan of OIDC tokens there is no immediate
	// locking here, only some long term assertion that future
	// activity may be prevented after some time window has
	// elapsed.
	SetUserActive(user string, active bool) error
}

// lavaClient implements LAVAer interface.
// This structure contains only the data required to interact with LAVA instance.
type lavaClient struct {
	client *xmlrpc.Client
}

func newLavaClient(secret, callback_url string) (*lavaClient, error) {
	headers := make(map[string]string)
	encoded_secret := b64.StdEncoding.EncodeToString([]byte(secret))
	headers["Authorization"] = fmt.Sprintf("Basic %s", encoded_secret)

	// We're starting with the callback URL (something like
	// http://lava/oidc/callback) and the RPC2 endpoint is going
	// to be a peer URL (something like http://lava/RPC2).  We
	// could just use the hostname, but the actual thing that's
	// guaranteed is that the RPC2 URL will appear alongside the
	// oidc URL. So safer to strip the suffix from the path. That
	// means if you're using http://womble/my/lava/ it will still
	// work.
	rpc_url, err := url.Parse(callback_url)
	if err != nil {
		return nil, fmt.Errorf("failed to parse LAVA callback url: %w", err)
	}

	rpc_url.Path = strings.TrimSuffix(rpc_url.Path, "/oidc/callback") + "/RPC2"

	client, err := xmlrpc.NewClient(rpc_url.String(), xmlrpc.Headers(headers))
	if err != nil {
		return nil, fmt.Errorf("failed to create XMLRPC client: %w", err)
	}
	return &lavaClient{client}, nil
}

func (lc *lavaClient) GetGroups(user string) ([]string, error) {
	params := struct {
		User   string   `xmlrpc:"user"`
	}{
		User:   user,
	}
	result := struct {
		Groups []string
	}{}
	if err := lc.client.Call("system.get_user_groups", &params, &result); err != nil {
		return nil, err
	}
	return result.Groups, nil
}

func (lc *lavaClient) SetGroups(user string, groups []string) error {
	params := struct {
		User   string   `xmlrpc:"user"`
		Groups []string `xmlrpc:"groups"`
	}{
		User:   user,
		Groups: groups,
	}
	if err := lc.client.Call("system.set_user_groups", &params, nil); err != nil {
		return err
	}
	return nil
}

func (lc *lavaClient) GetAllUsers() ([]string, error) {
	result := struct {
		Users []string
	}{}

	if err := lc.client.Call("system.get_all_users", nil, &result); err != nil {
		return nil, err
	}
	return result.Users, nil
}

func (lc *lavaClient) SetUserActive(user string, active bool) error {
	params := struct {
		User string `xmlrpc:"user"`
		Active bool `xmlrpc:"active"`
	}{
		User: user,
		Active: active,
	}

	if err := lc.client.Call("system.set_user_active", &params, nil); err != nil {
		return err
	}
	return nil
}
