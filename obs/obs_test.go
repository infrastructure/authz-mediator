// Copyright (C) 2022, Andrej Shadura
// Copyright (C) 2022, Collabora Limited
//
// SPDX-License-Identifier: Apache-2.0

package obs_test

import (
	"fmt"
	"sort"

	go_obs "github.com/andrewshadura/go-obs"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"gitlab.apertis.org/infrastructure/authz-mediator/claims"
	"gitlab.apertis.org/infrastructure/authz-mediator/config"
	"gitlab.apertis.org/infrastructure/authz-mediator/obs"
)

const (
	existingEmail = "correct@apert.is"
	existingUser  = "apertis"
)

type fakeObser struct {
	groups     []string
	userGroups []string
}

func (obser *fakeObser) GetUser(email string) (*go_obs.User, error) {
	GinkgoWriter.Println("get user by email", email)
	if email == existingEmail {
		return &go_obs.User{
			ID:    existingUser,
			Email: existingEmail,
		}, nil
	} else {
		return nil, fmt.Errorf("no users found with email %s", email)
	}
}

func (obser *fakeObser) GetGroup(name string) (*go_obs.Group, error) {
	GinkgoWriter.Println("get group", name)
	for _, g := range obser.groups {
		if g == name {
			return &go_obs.Group{
				ID: name,
			}, nil
		}
	}
	return nil, fmt.Errorf("no group: %s", name)
}

func (obser *fakeObser) AddGroupMember(gid, uid string) error {
	GinkgoWriter.Println("add", uid, "to group", gid)
	if uid != existingUser {
		return fmt.Errorf("no user: %s", uid)
	}

	for _, g := range obser.userGroups {
		if g == gid {
			return nil
		}
	}

	obser.userGroups = append(obser.userGroups, gid)
	return nil
}

func (obser *fakeObser) RemoveGroupMember(gid, uid string) error {
	GinkgoWriter.Println("remove", uid, "from group", gid)
	if uid != existingUser {
		return fmt.Errorf("no user: %s", uid)
	}

	newUserGroups := []string{}

	for _, g := range obser.userGroups {
		if g != gid {
			newUserGroups = append(newUserGroups, g)
		}
	}

	obser.userGroups = newUserGroups
	return nil
}

func (obser *fakeObser) GetUserGroups(uid string) ([]string, error) {
	GinkgoWriter.Println("get groups for user", uid)
	if uid == existingUser {
		GinkgoWriter.Println(" ->", obser.userGroups)
		return obser.userGroups, nil
	} else {
		return []string{}, fmt.Errorf("no user: %s", uid)
	}
}

func (obser *fakeObser) GetUserByUsername(username string) (*go_obs.User, error) {
	GinkgoWriter.Println("get user by username", username)
	if username == existingUser {
		return &go_obs.User{
			ID:    existingUser,
			Email: existingEmail,
		}, nil
	} else {
		return nil, fmt.Errorf("no users found with username %s", username)
	}

}

// The two functions below are not used in the OBS backend test
func (obser *fakeObser) GetAllUsers() ([]string, error) {
	return nil, fmt.Errorf("Not implemented")
}

func (obser *fakeObser) LockUser(user string) error {
	return fmt.Errorf("Not implemented")
}

var _ = Describe("Obs", func() {
	var (
		o     *obs.OBS
		obser *fakeObser
		err   error
		conf  config.Config
	)

	BeforeEach(func() {
		obser = &fakeObser{
			groups: []string{
				"group1",
				"group2",
				"group3",
			},
		}
		o = &obs.OBS{obser}
		conf = config.Config{
			Retries: 1,
			BackOff: 1,
			IncludedGroups: []string{
				"group*",
			},
		}
	})

	Describe("Apply", func() {
		Context("when claims are nil", func() {
			It("should fail and return an error", func() {
				err = o.Apply(nil, &config.Config{})
				Expect(err).To(HaveOccurred())
				Expect(err.Error()).To(Equal("no claims to apply"))
			})
		})

		Context("when there is no user for given email", func() {
			It("should fail and return an error", func() {
				err = o.Apply(&claims.Claims{Email: ""}, &config.Config{Retries: 1})
				Expect(err).To(HaveOccurred())
				Expect(err.Error()).To(ContainSubstring("no user"))
			})
		})

		Context("when claims have group lists for a new user", func() {
			initialGroups := []string{
				"group1", "group2", "missinggroup",
			}
			initialExistingGroups := []string{
				"group1", "group2",
			}
			newGroups := []string{
				"group2", "group3",
			}
			allGroups := []string{
				"group1", "group2", "group3",
			}

			It("add the user to existing groups, then when new claim is applied, remove the user from the old ones", func() {
				By("Processing the first groups claim")
				err = o.Apply(&claims.Claims{
					Email:  existingEmail,
					Username: existingUser + "@example.org",
					Groups: initialGroups,
				}, &conf)
				Expect(err).ToNot(HaveOccurred())

				sort.Strings(obser.groups)
				sort.Strings(obser.userGroups)

				Expect(obser.userGroups).To(Equal(initialExistingGroups))

				By("Processing the second groups claim")
				err = o.Apply(&claims.Claims{
					Email:  existingEmail,
					Username: existingUser,
					Groups: newGroups,
				}, &conf)
				Expect(err).ToNot(HaveOccurred())

				sort.Strings(obser.groups)
				sort.Strings(obser.userGroups)

				Expect(obser.groups).To(Equal(allGroups))
				Expect(obser.userGroups).To(Equal(newGroups))
			})

		})

	})

	Describe("GetUser", func() {
		var u *go_obs.User

		Context("when requested email is invalid", func() {
			It("should fail and return an error", func() {
				u, err = o.GetUser("")
				Expect(err).To(HaveOccurred())
				Expect(err.Error()).To(ContainSubstring("no user"))
				Expect(u).To(BeNil())
			})
		})

		Context("when requested email is correct", func() {
			It("should return correct user", func() {
				u, err = o.GetUser(existingEmail)
				Expect(err).ToNot(HaveOccurred())
				Expect(u.ID).To(Equal(existingUser))
			})
		})
	})

})
