// Copyright (C) 2021, Paweł Wieczorek
// Copyright (C) 2021, Collabora Limited
//
// SPDX-License-Identifier: Apache-2.0

package obs

import (
	"fmt"
	"os"
	"strings"

	"github.com/andrewshadura/go-obs"
	"github.com/apex/log"

	"gitlab.apertis.org/infrastructure/authz-mediator/config"
)

// OBSer provides passthrough interface to the OBS API.
// It does not handle any errors. It only ensures passed arguments comply with OBS API requirements.
//
// Mocking this interface allows testing higher level logic without OBS instance.
type OBSer interface {
	// following example is a subject to change
	GetUser(string) (*obs.User, error)
	GetUserByUsername(username string) (*obs.User, error)
	GetGroup(string) (*obs.Group, error)
	AddGroupMember(gid, uid string) error
	RemoveGroupMember(gid, uid string) error
	GetUserGroups(uid string) ([]string, error)
	// Get all users (returns user IDs)
	GetAllUsers() ([]string, error)
	// Lock a user by their user ID
	LockUser(uid string) error
}

// obsClient implements OBSer interface.
// This structure contains only the data required to interact with OBS instance.
type obsClient struct {
	client *obs.Client
}

func newObsClient(secret, url string) (*obsClient, error) {
	secretParts := strings.Split(secret, ":")
	if len(secretParts) == 1 {
		return nil, fmt.Errorf("OBS password missing")
	}

	client, err := obs.NewClient(secretParts[0], secretParts[1], obs.WithBaseURL(url))
	if err != nil {
		return nil, fmt.Errorf("failed to create OBS client: %v", err)
	}
	if insecure_https, ok := os.LookupEnv("OBS_INSECURE_HTTPS"); ok {
		if config.Truthy(insecure_https) {
			client.InsecureSkipVerify = true
			log.Warn("not verifying OBS API HTTPS certificates (not for production use!)")
		}
	}

	return &obsClient{client}, nil
}

func (oc *obsClient) GetUser(email string) (*obs.User, error) {
	return oc.client.GetUserByEmail(email)
}

func (oc *obsClient) GetUserByUsername(username string) (*obs.User, error) {
	return oc.client.GetUser(username)
}

func (oc *obsClient) GetGroup(name string) (*obs.Group, error) {
	return oc.client.GetGroup(name)
}

func (oc *obsClient) AddGroupMember(gid, uid string) error {
	return oc.client.AddGroupMember(gid, uid)
}

func (oc *obsClient) RemoveGroupMember(gid, uid string) error {
	return oc.client.RemoveGroupMember(gid, uid)
}

func (oc *obsClient) GetUserGroups(uid string) ([]string, error) {
	return oc.client.GetUserGroups(uid)
}

func (oc *obsClient) GetAllUsers() ([]string, error) {
	// TODO: Only return active users
	users, err := oc.client.ListUsers("")
	if err != nil {
		return nil, err
	}

	usernames := []string{}
	for _, u := range users {
		// _nobody_ is an internal "anonymous" user in OBS, don't touch it
		if u != "_nobody_" {
			usernames = append(usernames, strings.ToLower(u))
		}
	}

	return usernames, nil
}

func (oc *obsClient) LockUser(uid string) error {
	return oc.client.LockUser(uid)
}
