// Copyright (C) 2021,      Paweł Wieczorek
// Copyright (C) 2021-2022, Andrej Shadura
// Copyright (C) 2021—2022, Collabora Limited
//
// SPDX-License-Identifier: Apache-2.0

package obs

import (
	"fmt"

	"github.com/andrewshadura/go-obs"
	"github.com/apex/log"
	mapset "github.com/deckarep/golang-set/v2"

	. "gitlab.apertis.org/infrastructure/authz-mediator/claims"
	"gitlab.apertis.org/infrastructure/authz-mediator/config"
)

// OBS handles service interaction logic.
// It provides only high level functions, handles possible errors and verifies which ones can be safely ignored.
// This structure contains no data to access OBS instance nor methods to call its API directly.
type OBS struct {
	OBSer
}

func NewOBS(secret, url string) (*OBS, error) {
	oc, err := newObsClient(secret, url)
	if err != nil {
		return nil, fmt.Errorf("failed to create OBS backend: %w", err)
	}
	return &OBS{oc}, nil
}

func (obs *OBS) Apply(claims *Claims, config *config.Config) error {
	if claims == nil {
		return fmt.Errorf("no claims to apply")
	}

	user, err := obs.FindUser(claims.Email, claims.Username)
	if err != nil {
		return fmt.Errorf("failed to get user: %w", err)
	}

	newGroups := mapset.NewSet[string](claims.Groups...)
	existingGroups := mapset.NewSet[string]()
	if groups, err := obs.GetUserGroups(user.ID); err == nil {
		groups = FilterGroups(groups, config.IncludedGroups, []string{})
		existingGroups = mapset.NewSet[string](groups...)
	}

	logctx := log.WithFields(log.Fields{
		"backend": "obs",
		"user":    user.ID,
	})

	to_add := newGroups.Difference(existingGroups)
	to_remove := existingGroups.Difference(newGroups)

	logctx.WithFields(log.Fields{
		"existing_groups": existingGroups,
		"to_add":		   to_add,
		"to_remove":	   to_remove,
	}).Debug("group membership changes")

	for gc := range to_add.Iter() {
		group, err := obs.GetGroup(gc)
		if err != nil {
			logctx.WithField("group", gc).WithError(err).Warn("non-existent group, not adding")
			continue
		}

		logctx.WithField("group", group.ID).Info("adding user")
		err = obs.AddMember(group.ID, user.ID)
		if err != nil {
			return fmt.Errorf("failed to add user %s to group %s: %w", user.ID, group.ID, err)
		}
	}

	for gc := range to_remove.Iter() {
		group, err := obs.GetGroup(gc)
		if err == nil {
			logctx.WithField("group", group.ID).Info("removing user")
			err = obs.RemoveGroupMember(group.ID, user.ID)
			if err != nil {
				return fmt.Errorf("failed to remove user %s from group %s: %w", user.ID, group.ID, err)
			}
		}
	}

	return nil
}

func (obs *OBS) FindUser(email string, username string) (*obs.User, error) {
	// Azure sets username as <userid>@<domain>
	localPart := UsernameLocalPart(username)
	user, err := obs.GetUserByUsername(localPart)
	if user != nil {
		return user, nil
	}

	return nil, fmt.Errorf("failed to get OBS user: %w", err)
}

func (obs *OBS) AddMember(gid, uid string) error {
	if err := obs.AddGroupMember(gid, uid); err != nil {
		return fmt.Errorf("failed to add OBS group member: %w", err)
	}
	return nil
}
