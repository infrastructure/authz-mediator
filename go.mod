module gitlab.apertis.org/infrastructure/authz-mediator

go 1.18

require (
	alexejk.io/go-xmlrpc v0.4.0
	github.com/Azure/azure-sdk-for-go/sdk/azcore v1.5.0
	github.com/Azure/azure-sdk-for-go/sdk/azidentity v1.2.2
	github.com/andrewshadura/go-obs v0.0.0-20220828140654-d182bfe1048a
	github.com/apex/log v1.9.0
	github.com/coreos/go-oidc/v3 v3.5.0
	github.com/deckarep/golang-set/v2 v2.3.0
	github.com/mattn/go-isatty v0.0.18
	github.com/microsoftgraph/msgraph-sdk-go v0.63.0
	github.com/microsoftgraph/msgraph-sdk-go-core v0.36.1
	github.com/onsi/ginkgo/v2 v2.6.1
	github.com/onsi/gomega v1.24.2
	github.com/xanzy/go-gitlab v0.83.0
	golang.org/x/oauth2 v0.7.0
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/Azure/azure-sdk-for-go/sdk/internal v1.3.0 // indirect
	github.com/AzureAD/microsoft-authentication-library-for-go v1.0.0 // indirect
	github.com/cjlapao/common-go v0.0.39 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-jose/go-jose/v3 v3.0.0 // indirect
	github.com/go-logfmt/logfmt v0.6.0 // indirect
	github.com/go-logr/logr v1.2.4 // indirect
	github.com/go-logr/stdr v1.2.2 // indirect
	github.com/golang-jwt/jwt/v4 v4.5.0 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/google/go-cmp v0.5.9 // indirect
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/hashicorp/go-cleanhttp v0.5.2 // indirect
	github.com/hashicorp/go-retryablehttp v0.7.2 // indirect
	github.com/kylelemons/godebug v1.1.0 // indirect
	github.com/microsoft/kiota-abstractions-go v0.19.1 // indirect
	github.com/microsoft/kiota-authentication-azure-go v0.6.0 // indirect
	github.com/microsoft/kiota-http-go v0.16.2 // indirect
	github.com/microsoft/kiota-serialization-form-go v0.9.1 // indirect
	github.com/microsoft/kiota-serialization-json-go v0.9.1 // indirect
	github.com/microsoft/kiota-serialization-text-go v0.7.1 // indirect
	github.com/pkg/browser v0.0.0-20210911075715-681adbf594b8 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.8.2 // indirect
	github.com/yosida95/uritemplate/v3 v3.0.2 // indirect
	go.opentelemetry.io/otel v1.14.0 // indirect
	go.opentelemetry.io/otel/trace v1.14.0 // indirect
	golang.org/x/crypto v0.8.0 // indirect
	golang.org/x/net v0.9.0 // indirect
	golang.org/x/sys v0.7.0 // indirect
	golang.org/x/text v0.9.0 // indirect
	golang.org/x/time v0.3.0 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/protobuf v1.30.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
