import os

import pytest
from gitlab.exceptions import GitlabCreateError

import gitlab


def pytest_addoption(parser):
    parser.addoption("--service-url", action="store")
    parser.addoption("--service-external-url", action="store")


@pytest.fixture(scope="session")
def service_url(request):
    service_url_value = request.config.option.service_url
    return service_url_value


@pytest.fixture(scope="session")
def service_external_url(request):
    service_external_url_value = request.config.option.service_external_url
    return service_external_url_value


@pytest.fixture(scope="session")
def gl_token():
    return os.environ.get("GITLAB_SECRET")


def _gitlab_ensure_group(gl, group, parent=None):
    data = {"name": group, "path": group}
    if parent is not None:
        data["parent_id"] = parent.id
    try:
        g = gl.groups.create(data)
    except GitlabCreateError as e:
        if ':path=>["has already been taken"]' not in e.error_message:
            raise e
        prefix = f"{parent.full_path}/" if parent is not None else ""
        g = gl.groups.get(prefix + group)
    return g


@pytest.fixture(scope="session")
def gl(service_external_url, gl_token):
    gl_client = gitlab.Gitlab(service_external_url, private_token=gl_token)
    parent = _gitlab_ensure_group(gl_client, "mediated")
    for group in ["maintainers", "developers"]:
        _gitlab_ensure_group(gl_client, group, parent)
    return gl_client
