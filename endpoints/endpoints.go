// Copyright (C) 2021, Paweł Wieczorek
// Copyright (C) 2021, Collabora Limited
//
// SPDX-License-Identifier: Apache-2.0

package endpoints

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"
)

type Endpoints struct {
	Issuer     string   `json:"issuer,omitempty"`
	Auth       string   `json:"authorization_endpoint"`
	Token      string   `json:"token_endpoint"`
	PubKeys    string   `json:"jwks_uri"`
	UserInfo   string   `json:"userinfo_endpoint"`
	Scopes     []string `json:"scopes_supported,omitempty"`
	Claims     []string `json:"claims_supported,omitempty"`
	Algorithms []string `json:"id_token_signing_alg_values_supported,omitempty"`
}

const (
	MetadataPath = "/.well-known/openid-configuration"
)

func NewEndpoints(providerURL string, client *http.Client) (*Endpoints, error) {
	wellKnown := strings.TrimSuffix(providerURL, "/") + MetadataPath
	req, err := http.NewRequest(http.MethodGet, wellKnown, nil)
	if err != nil {
		return nil, err
	}

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read response body: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("%s: %s", resp.Status, body)
	}

	var e Endpoints
	err = json.Unmarshal(body, &e)
	if err != nil {
		return nil, fmt.Errorf("failed to unmarshal provider discovery object: %v", err)
	}

	return &e, nil
}
