// Copyright (C) 2021, Paweł Wieczorek
// Copyright (C) 2021, Collabora Limited
//
// SPDX-License-Identifier: Apache-2.0

package endpoints_test

import (
	"fmt"
	"net/http"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"github.com/onsi/gomega/ghttp"

	"gitlab.apertis.org/infrastructure/authz-mediator/endpoints"
)

var _ = Describe("Endpoints", func() {
	Describe("NewEndpoints", func() {
		var (
			server *ghttp.Server
		)

		BeforeEach(func() {
			server = ghttp.NewServer()
		})

		AfterEach(func() {
			server.Close()
		})

		Context("when provider URI is not parsable", func() {
			It("should return a parsing error", func() {
				ep, err := endpoints.NewEndpoints("\r\n", http.DefaultClient)
				Expect(err).To(HaveOccurred())
				Expect(err.Error()).To(Equal(fmt.Sprintf("parse \"\\r\\n%s\": net/url: invalid control character in URL", endpoints.MetadataPath)))
				Expect(ep).To(BeNil())
			})
		})

		Context("when discovery request cannot be made", func() {
			It("should return a request error", func() {
				host := "foo"
				service := fmt.Sprintf("https://%s", host)
				ep, err := endpoints.NewEndpoints(service, http.DefaultClient)
				Expect(err).To(HaveOccurred())
				Expect(err.Error()).To(HavePrefix(fmt.Sprintf("Get \"%s%s\": dial tcp: lookup %s", service, endpoints.MetadataPath, host)))
				Expect(err.Error()).To(HaveSuffix("no such host"))
				Expect(ep).To(BeNil())
			})
		})

		Context("when response status is not OK", func() {
			It("should return an error response from server", func() {
				server.AppendHandlers(
					ghttp.CombineHandlers(
						ghttp.VerifyRequest(http.MethodGet, endpoints.MetadataPath),
						ghttp.RespondWith(http.StatusBadRequest, "not OK"),
					),
				)
				ep, err := endpoints.NewEndpoints(server.URL(), http.DefaultClient)
				Expect(err).To(HaveOccurred())
				Expect(err.Error()).To(Equal("400 Bad Request: not OK"))
				Expect(ep).To(BeNil())
			})
		})

		Context("when response cannot be unmarshalled", func() {
			It("should return an unmarshalling error", func() {
				server.AppendHandlers(
					ghttp.CombineHandlers(
						ghttp.VerifyRequest(http.MethodGet, endpoints.MetadataPath),
						ghttp.RespondWithJSONEncoded(http.StatusOK, "foo"),
					),
				)
				ep, err := endpoints.NewEndpoints(server.URL(), http.DefaultClient)
				Expect(err).To(HaveOccurred())
				Expect(err.Error()).To(ContainSubstring("failed to unmarshal provider discovery object:"))
				Expect(ep).To(BeNil())
			})
		})

		Context("when proper request is made", func() {
			It("should return endpoints and no error", func() {
				eps := &endpoints.Endpoints{
					Auth:     "foo",
					Token:    "bar",
					PubKeys:  "baz",
					UserInfo: "quuz",
				}
				server.AppendHandlers(
					ghttp.CombineHandlers(
						ghttp.VerifyRequest(http.MethodGet, endpoints.MetadataPath),
						ghttp.RespondWithJSONEncoded(http.StatusOK, eps),
					),
				)
				ep, err := endpoints.NewEndpoints(server.URL(), http.DefaultClient)
				Expect(err).ToNot(HaveOccurred())
				Expect(ep).To(Equal(eps))
			})
		})
	})
})
