// Copyright (C) 2021, Paweł Wieczorek
// Copyright (C) 2021, Collabora Limited
//
// SPDX-License-Identifier: Apache-2.0

package config

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/apex/log"
	"gopkg.in/yaml.v2"
)

const (
	DefaultMediatorID            = "authz-mediator"
	DefaultProviderURL           = "http://localhost:5556/dex"
	DefaultMediatorURL           = "http://localhost:5555"
	DefaultRedirectURL           = "http://localhost/users/auth/openid_connect/callback"
	DefaultGroupsScope           = false
	DefaultAPIRetries            = 10
	DefaultAPIBackOff            = 500
	DefaultPort                  = 5555
	DefaultMediatorUsername      = "mediator"
	DefaultEnforceGroups         = false
	DefaultEnforceGroupsInterval = 15 * time.Minute

	MediatorIDEnv               = "AUTHZ_MEDIATOR_ID"
	MediatorBackendEnv          = "AUTHZ_MEDIATOR_BACKEND"
	ProviderURLEnv              = "AUTHZ_MEDIATOR_PROVIDER"
	MediatorURLEnv              = "AUTHZ_MEDIATOR_SELF"
	RedirectURLEnv              = "AUTHZ_MEDIATOR_REDIRECT"
	GroupsScopeEnv              = "AUTHZ_MEDIATOR_GROUPS"
	APIRetriesEnv               = "AUTHZ_MEDIATOR_RETRIES"
	APIBackOffEnv               = "AUTHZ_MEDIATOR_BACKOFF"
	PortEnv                     = "AUTHZ_MEDIATOR_PORT"
	RequiredGroupsEnv           = "AUTHZ_MEDIATOR_REQUIRED_GROUPS"
	IncludedGroupsEnv           = "AUTHZ_MEDIATOR_INCLUDED_GROUPS"
	GroupMapEnv                 = "AUTHZ_MEDIATOR_GROUP_MAP"
	GitlabParentGroupEnv        = "AUTHZ_MEDIATOR_GITLAB_PARENT_GROUP"
	EnforceGroupsEnv            = "AUTHZ_MEDIATOR_ENFORCE_GROUPS"
	EnforceGroupsIntervalEnv    = "AUTHZ_MEDIATOR_ENFORCE_GROUPS_INTERVAL"
	EnforceGroupsIgnoreUsersEnv = "AUTHZ_MEDIATOR_ENFORCE_GROUPS_IGNORE_USERS"
	EnforceGroupsAuditOnlyEnv   = "AUTHZ_MEDIATOR_ENFORCE_GROUPS_AUDIT_ONLY"
	MediatorUsernameEnv         = "AUTHZ_MEDIATOR_USERNAME"
)

type Config struct {
	ID                       string            `yaml:"id"`
	Backend                  string            `yaml:"backend"`
	Provider                 string            `yaml:"provider"`
	Mediator                 string            `yaml:"mediator"`
	Redirect                 string            `yaml:"redirect"`
	RequiredGroups           []string          `yaml:"required_groups"`
	IncludedGroups           []string          `yaml:"included_groups"`
	GroupsScope              bool              `yaml:"groups"`
	Retries                  int               `yaml:"retries"`
	BackOff                  int               `yaml:"backoff"`
	Port                     uint16            `yaml:"port"`
	GroupMap                 map[string]string `yaml:"group_map"`
	GitlabParentGroup        string            `yaml:"gitlab_parent_group"`
	MediatorUsername         string            `yaml:"mediator_username"`
	EnforceGroups            bool              `yaml:"enforce_groups"`
	EnforceGroupsInterval    time.Duration     `yaml:"enforce_groups_interval"`
	EnforceGroupsIgnoreUsers []string          `yaml:"enforce_groups_ignore_users"`
	EnforceGroupsAuditOnly   bool              `yaml:"enforce_groups_audit_only"`
}

func GetConfig(file string) (*Config, error) {
	data, err := os.ReadFile(file)
	if err != nil {
		return nil, fmt.Errorf("failed to read config file %s: %w", file, err)
	}

	var c Config
	err = yaml.Unmarshal(data, &c)
	if err != nil {
		return nil, fmt.Errorf("failed to parse config file %s: %w", file, err)
	}
	return &c, nil
}

func (c *Config) ApplyEnv() {
	if id, ok := os.LookupEnv(MediatorIDEnv); ok {
		c.ID = id
	}
	if backend, ok := os.LookupEnv(MediatorBackendEnv); ok {
		c.Backend = backend
	}
	if provider, ok := os.LookupEnv(ProviderURLEnv); ok {
		c.Provider = provider
	}
	if mediator, ok := os.LookupEnv(MediatorURLEnv); ok {
		c.Mediator = mediator
	}
	if redirect, ok := os.LookupEnv(RedirectURLEnv); ok {
		c.Redirect = redirect
	}
	if groups, ok := os.LookupEnv(GroupsScopeEnv); ok {
		c.GroupsScope = Truthy(groups)
	}
	if retries, ok := os.LookupEnv(APIRetriesEnv); ok {
		arg, err := strconv.Atoi(retries)
		if err != nil {
			arg = 0
		}
		c.Retries = arg
	}
	if backoff, ok := os.LookupEnv(APIBackOffEnv); ok {
		arg, err := strconv.Atoi(backoff)
		if err != nil {
			arg = 0
		}
		c.BackOff = arg
	}
	if port, ok := os.LookupEnv(PortEnv); ok {
		arg, err := strconv.ParseUint(port, 10, 16)
		if err != nil {
			arg = 0
		}
		c.Port = uint16(arg)
	}
	if required_groups, ok := os.LookupEnv(RequiredGroupsEnv); ok {
		arg := strings.Split(required_groups, " ")
		c.RequiredGroups = arg
	}

	if included_groups, ok := os.LookupEnv(IncludedGroupsEnv); ok {
		arg := strings.Split(included_groups, " ")
		c.IncludedGroups = arg
	}

	if groupmap, ok := os.LookupEnv(GroupMapEnv); ok {
		arg := make(map[string]string)
		err := yaml.Unmarshal([]byte(groupmap), arg)
		if err == nil {
			c.GroupMap = arg
		}
	}

	if parent_group, ok := os.LookupEnv(GitlabParentGroupEnv); ok {
		c.GitlabParentGroup = parent_group
	}

	if mediator_username, ok := os.LookupEnv(MediatorUsernameEnv); ok {
		c.MediatorUsername = mediator_username
	}

	if enforce_groups, ok := os.LookupEnv(EnforceGroupsEnv); ok {
		c.EnforceGroups = Truthy(enforce_groups)
	}

	if enforce_groups_interval, ok := os.LookupEnv(EnforceGroupsIntervalEnv); ok {
		c.EnforceGroupsInterval, _ = time.ParseDuration(enforce_groups_interval)
	}

	if enforce_groups_ignore_users, ok := os.LookupEnv(EnforceGroupsIgnoreUsersEnv); ok {
		arg := strings.Split(enforce_groups_ignore_users, " ")
		c.EnforceGroupsIgnoreUsers = arg
	}

	if enforce_groups_audit_only, ok := os.LookupEnv(EnforceGroupsAuditOnlyEnv); ok {
		c.EnforceGroupsAuditOnly = Truthy(enforce_groups_audit_only)
	}
}

func (c *Config) ApplyDefaults() {
	if c.ID == "" {
		c.ID = DefaultMediatorID
	}
	if c.Provider == "" {
		c.Provider = DefaultProviderURL
	}
	if c.Mediator == "" {
		c.Mediator = DefaultMediatorURL
	}
	if c.Redirect == "" {
		c.Redirect = DefaultRedirectURL
	}
	// GroupsScope is false by default unless explicitly set
	if c.Retries == 0 {
		c.Retries = DefaultAPIRetries
	}
	if c.BackOff == 0 {
		c.BackOff = DefaultAPIBackOff
	}
	if c.Port == 0 {
		c.Port = DefaultPort
	}
	// When group enforcement is disabled, we still allow ticks to happen,
	// but at the default interval to prevent invalid configuration
	// from interfering with the rest of mediator
	if c.EnforceGroupsInterval == 0 || !c.EnforceGroups {
		c.EnforceGroupsInterval = DefaultEnforceGroupsInterval
	}
	if c.EnforceGroupsInterval < time.Minute {
		log.WithField("enforce_groups_interval", c.EnforceGroupsInterval).
			Warnf("Enforcement interval too short, defaulting to %v", DefaultEnforceGroupsInterval)
		c.EnforceGroupsInterval = DefaultEnforceGroupsInterval
	}
	if c.MediatorUsername == "" {
		c.MediatorUsername = DefaultMediatorUsername
	}
}

func Truthy(input string) bool {
	arg := strings.ToLower(input)
	if arg == "yes" || arg == "true" || arg == "1" {
		return true
	}
	return false
}
