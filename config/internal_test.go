package config

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

var _ = Describe("Internal", func() {
	Describe("Truthy", func() {
		Context("when argument should not be mapped to Truthy value", func() {
			It("should map empty string to false", func() {
				actual := Truthy("")
				Expect(actual).To(BeFalse())
			})

			DescribeTable("Truthy should map falsy values to false",
				func(arg string) {
					Expect(Truthy(arg)).To(BeFalse())
				},
				Entry("is 0", "0"),
				Entry("is no", "no"),
				Entry("is No", "No"),
				Entry("is NO", "NO"),
				Entry("is false", "false"),
				Entry("is False", "False"),
				Entry("is FALSE", "FALSE"),
			)
		})

		Context("when argument can be mapped to Truthy value", func() {
			DescribeTable("Truthy should map truthy values to true",
				func(arg string) {
					Expect(Truthy(arg)).To(BeTrue())
				},
				Entry("is 1", "1"),
				Entry("is yes", "yes"),
				Entry("is Yes", "Yes"),
				Entry("is YES", "YES"),
				Entry("is true", "true"),
				Entry("is True", "True"),
				Entry("is TRUE", "TRUE"),
			)
		})
	})
})
