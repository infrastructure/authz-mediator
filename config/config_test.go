// Copyright (C) 2021, Paweł Wieczorek
// Copyright (C) 2021, Collabora Limited
//
// SPDX-License-Identifier: Apache-2.0

package config_test

import (
	"os"
	"strconv"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"gitlab.apertis.org/infrastructure/authz-mediator/config"
)

var _ = Describe("Config", func() {
	const (
		foo  = "foo"
		bar  = "bar"
		baz  = "baz"
		q2z  = "quuz"
		reps = 5
		time = 10
	)

	var (
		conf       *config.Config
		sampleConf *config.Config
		otherConf  *config.Config
	)

	BeforeEach(func() {
		conf = &config.Config{}
		sampleConf = &config.Config{
			ID:          q2z,
			Provider:    foo,
			Mediator:    bar,
			Redirect:    baz,
			GroupsScope: true,
			Retries:     reps,
			BackOff:     time,
		}
		otherConf = &config.Config{
			ID:          baz,
			Provider:    bar,
			Mediator:    foo,
			Redirect:    q2z,
			GroupsScope: false,
			Retries:     reps * 2,
			BackOff:     time * 2,
		}
	})

	Describe("ApplyEnv", func() {
		Context("when there is no relevant configuration set in env", func() {
			Context("when config is empty", func() {
				BeforeEach(func() {
					conf.ApplyEnv()
				})

				It("should leave config unchanged", func() {
					Expect(conf).To(Equal(&config.Config{}))
				})
			})

			Context("when config is set", func() {
				BeforeEach(func() {
					conf = &config.Config{
						ID:          q2z,
						Provider:    foo,
						Mediator:    bar,
						Redirect:    baz,
						GroupsScope: true,
						Retries:     reps,
						BackOff:     time,
					}
					conf.ApplyEnv()
				})

				It("should leave config unchanged", func() {
					Expect(conf).To(Equal(sampleConf))
				})
			})
		})

		Context("when there is relevant configuration set in env", func() {
			BeforeEach(func() {
				os.Setenv(config.MediatorIDEnv, q2z)
				os.Setenv(config.ProviderURLEnv, foo)
				os.Setenv(config.MediatorURLEnv, bar)
				os.Setenv(config.RedirectURLEnv, baz)
				os.Setenv(config.GroupsScopeEnv, "true")
				os.Setenv(config.APIRetriesEnv, strconv.Itoa(reps))
				os.Setenv(config.APIBackOffEnv, strconv.Itoa(time))
				conf.ApplyEnv()
			})

			AfterEach(func() {
				os.Unsetenv(config.MediatorIDEnv)
				os.Unsetenv(config.ProviderURLEnv)
				os.Unsetenv(config.MediatorURLEnv)
				os.Unsetenv(config.RedirectURLEnv)
				os.Unsetenv(config.GroupsScopeEnv)
				os.Unsetenv(config.APIRetriesEnv)
				os.Unsetenv(config.APIBackOffEnv)
			})

			Context("when config is empty", func() {
				It("should apply config from env", func() {
					Expect(conf).To(Equal(sampleConf))
				})
			})

			Context("when config is set", func() {
				BeforeEach(func() {
					conf = otherConf
					Expect(conf).ToNot(Equal(sampleConf))
					conf.ApplyEnv()
				})

				It("should apply config from env", func() {
					Expect(conf).To(Equal(sampleConf))
				})
			})
		})

		Context("when there is relevant but malformed configuration set in env", func() {
			BeforeEach(func() {
				os.Setenv(config.APIRetriesEnv, "+")
				os.Setenv(config.APIBackOffEnv, "-")
				conf.ApplyEnv()
			})

			AfterEach(func() {
				os.Unsetenv(config.APIRetriesEnv)
				os.Unsetenv(config.APIBackOffEnv)
			})

			Context("when config is empty", func() {
				It("should leave config unchanged", func() {
					Expect(conf).To(Equal(&config.Config{}))
				})
			})

			Context("when config is set", func() {
				BeforeEach(func() {
					conf = otherConf
					Expect(conf).ToNot(Equal(sampleConf))
					conf.ApplyEnv()
				})

				It("should apply config defaults", func() {
					Expect(conf.Retries).To(BeZero())
					Expect(conf.BackOff).To(BeZero())
				})
			})
		})
	})

	Describe("ApplyDefaults", func() {
		Context("when config is missing fields", func() {
			BeforeEach(func() {
				conf.ApplyDefaults()
			})

			It("should fill config with defaults", func() {
				Expect(conf.ID).To(Equal(config.DefaultMediatorID))
				Expect(conf.Provider).To(Equal(config.DefaultProviderURL))
				Expect(conf.Mediator).To(Equal(config.DefaultMediatorURL))
				Expect(conf.Redirect).To(Equal(config.DefaultRedirectURL))
				Expect(conf.GroupsScope).To(BeFalse())
				Expect(conf.Retries).To(Equal(config.DefaultAPIRetries))
				Expect(conf.BackOff).To(Equal(config.DefaultAPIBackOff))
			})
		})

		Context("when config is already filled out", func() {
			BeforeEach(func() {
				conf = otherConf
				Expect(conf).ToNot(Equal(sampleConf))
				conf.ApplyDefaults()
			})

			It("should leave config unchanged", func() {
				Expect(conf).To(Equal(otherConf))
			})
		})
	})
})
