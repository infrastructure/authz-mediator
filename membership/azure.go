// Copyright (C) 2022, Andrej Shadura
// Copyright (C) 2022, Collabora Limited
//
// SPDX-License-Identifier: Apache-2.0

package membership

import (
	"context"
	"fmt"
	"path"
	"strings"

	"github.com/apex/log"

	"github.com/Azure/azure-sdk-for-go/sdk/azcore"
	"github.com/Azure/azure-sdk-for-go/sdk/azidentity"
	msgraphsdk "github.com/microsoftgraph/msgraph-sdk-go"
	msgraphcore "github.com/microsoftgraph/msgraph-sdk-go-core"
	"github.com/microsoftgraph/msgraph-sdk-go/groups"
	"github.com/microsoftgraph/msgraph-sdk-go/models"
)

// A group or a user
type Member struct {
	// Display name of a member
	DisplayName string
	// Short name of a member, e.g. a username
	Name string
	// Email
	Email string
	// Internal ID of a member (a UUID)
	ID string
}

type AzureClient struct {
	client *msgraphsdk.GraphServiceClient
}

// Create a new Azure MS Graph API client
// Pass an empty clientSecret to use "az cli" as a credential provider
func NewAzureClient(tenantID string, clientID string, clientSecret string) (*AzureClient, error) {
	var cred azcore.TokenCredential
	var err error

	if clientSecret == "" {
		cred, err = azidentity.NewDefaultAzureCredential(nil)
	} else {
		cred, err = azidentity.NewClientSecretCredential(
			tenantID,
			clientID,
			clientSecret,
			&azidentity.ClientSecretCredentialOptions{},
		)
	}

	if err != nil {
		return nil, &FieldableError{err}
	}

	client, err := msgraphsdk.NewGraphServiceClientWithCredentials(cred, nil)

	if err != nil {
		return nil, &FieldableError{err}
	}

	return &AzureClient{
		client: client,
	}, nil
}

// Return all groups
func (c *AzureClient) Groups() ([]Member, error) {
	return c.GroupsMatching([]string{"*"})
}

// Produce OData 4.0 query based on a list of fnmatch patterns
// This conversion is not precise, it only serves to reduce the
// amount of produced data, which still needs to be filtered
// using the original patterns.
//
// The exact syntax of the query is described in this document:
// https://docs.oasis-open.org/odata/odata/v4.0/errata03/os/complete/part2-url-conventions/odata-v4.0-errata03-os-part2-url-conventions-complete.html#_Toc453752356
func PatternsToFilter(field string, patterns []string) *string {
	filters := []string{}
	for _, pattern := range patterns {
		// group_prefix* -> startswith(field, 'group')
		prefix, _, _ := strings.Cut(pattern, "*")
		// we don’t allow / in queries to avoid having to escape them
		prefix, _, _ = strings.Cut(prefix, "/")
		// we don’t allow ' in queries to avoid having to escape them
		prefix, _, _ = strings.Cut(prefix, "'")
		if len(prefix) == 0 {
			// starts with *, have to match all
			// TODO: add endswith?
			return nil
		}
		filters = append(filters, fmt.Sprintf("startswith(%s,'%s')", field, prefix))
	}

	result := strings.Join(filters, " OR ")
	return &result
}

// Return groups matching at least one of the patterns
// This function pages through a multipage response automatically.
func (c *AzureClient) GroupsMatching(patterns []string) ([]Member, error) {
	requestParameters := &groups.GroupsRequestBuilderGetQueryParameters{
		Filter: PatternsToFilter("mailNickname", patterns),
	}
	configuration := &groups.GroupsRequestBuilderGetRequestConfiguration{
		QueryParameters: requestParameters,
	}

	result, err := c.client.Groups().Get(context.Background(), configuration)

	if err != nil {
		return nil, &FieldableError{err}
	}

	var groups []Member

	iterator, err := msgraphcore.NewPageIterator[models.Groupable](
		result,
		c.client.GetAdapter(),
		models.CreateGroupCollectionResponseFromDiscriminatorValue,
	)

	if err != nil {
		return nil, &FieldableError{err}
	}

	err = iterator.Iterate(context.Background(), func(v models.Groupable) bool {
		group := Member{}

		group.ID = *v.GetId()

		dn := v.GetDisplayName()
		if dn != nil {
			group.DisplayName = *dn
		}

		nn := v.GetMailNickname()
		if nn != nil {
			group.Name = strings.ToLower(*nn)
		}

		for _, pattern := range patterns {
			if match, _ := path.Match(pattern, group.Name); match {
				log.WithField("group", group).Debug("match")
				groups = append(groups, group)
				break
			}
		}

		return true
	})

	if err != nil {
		return nil, &FieldableError{err}
	}

	return groups, nil
}

// Return all members of a group by its ID (which is UUID, not a username)
// This function pages through a multipage response automatically.
// The result is a mapping of "principal names" to email addresses.
// Principal names are usually short email addresses, e.g. username@domain.
func (c *AzureClient) GroupMembers(groupID string) (map[string]string, error) {
	result, err := c.client.Groups().ByGroupId(groupID).Members().Get(context.Background(), nil)

	if err != nil {
		return nil, &FieldableError{err}
	}

	users := map[string]string{}

	iterator, err := msgraphcore.NewPageIterator[models.Userable](
		result,
		c.client.GetAdapter(),
		models.CreateUserCollectionResponseFromDiscriminatorValue,
	)

	if err != nil {
		return nil, &FieldableError{err}
	}

	err = iterator.Iterate(context.Background(), func(v models.Userable) bool {
		user := Member{}
		user.ID = *v.GetId()

		dn := v.GetDisplayName()
		if dn != nil {
			user.DisplayName = *dn
		}

		nn := v.GetUserPrincipalName()
		if nn != nil {
			user.Name = strings.ToLower(*nn)
		}

		m := v.GetMail()
		if m != nil {
			user.Email = strings.ToLower(*m)
		}

		log.WithField("user", user).WithField("group_id", groupID).Debug("member")

		users[user.Name] = user.Email

		return true
	})

	if err != nil {
		return nil, &FieldableError{err}
	}

	return users, nil
}

func (c *AzureClient) Client() *msgraphsdk.GraphServiceClient {
	return c.client
}
