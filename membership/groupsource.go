package membership

import (
	"fmt"
	"net/url"
	"strings"

	"gitlab.apertis.org/infrastructure/authz-mediator/config"
)

type GroupSource interface {
	// Request all groups
	Groups() ([]Member, error)
	// Request all groups matching patterns
	GroupsMatching(patterns []string) ([]Member, error)
	// Request members of a group specified by id
	GroupMembers(groupId string) (map[string]string, error)
}

func NewAzureGroupSource(config *config.Config, secret string) (GroupSource, error) {
	provider, err := url.Parse(config.Provider)
	if err != nil {
		return nil, err
	}
	if provider.Host != "login.microsoftonline.com" {
		return nil, fmt.Errorf("Provider is not login.microsoftonline.com: %s", config.Provider)
	}
	// Provider URLs look like: https://login.microsoftonline.com/<uuid>/v2.0
	tenantID := strings.SplitN(provider.Path, "/", 3)[1]
	clientID := config.ID

	return NewAzureClient(tenantID, clientID, secret)
}
