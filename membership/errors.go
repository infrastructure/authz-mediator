package membership

import (
	"github.com/apex/log"
	"github.com/microsoftgraph/msgraph-sdk-go/models/odataerrors"
)

type FieldableError struct {
	err error
}

func (e *FieldableError) Error() string {
	return e.err.Error()
}

func (e *FieldableError) Unwrap() error {
	return e.err
}

func (e *FieldableError) Fields() log.Fields {
	switch e.err.(type) {
	case *odataerrors.ODataError:
		typed := e.err.(*odataerrors.ODataError)
		if terr := typed.GetError(); terr != nil {
			return log.Fields{
				"code":    *terr.GetCode(),
				"message": *terr.GetMessage(),
			}
		}
	}
	return log.Fields{}
}
