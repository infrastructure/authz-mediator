// Copyright (C) 2021,      Paweł Wieczorek
// Copyright (C) 2021-2022, Andrej Shadura
// Copyright (C)      2022, Ryan Gonzalez
// Copyright (C) 2021—2022, Collabora Limited
//
// SPDX-License-Identifier: Apache-2.0

package gitlab_test

import (
	"fmt"
	"net/http"
	"net/url"

	ggl "github.com/xanzy/go-gitlab"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"gitlab.apertis.org/infrastructure/authz-mediator/claims"
	"gitlab.apertis.org/infrastructure/authz-mediator/config"
	"gitlab.apertis.org/infrastructure/authz-mediator/gitlab"
)

const (
	correctEmail   = "correct@apert.is"
	ambiguousEmail = "ambiguous@apert.is"

	correctUsername   = "correct"

	correctGroup      = "correct"
	correctChildGroup = "child"
	inaccessibleGroup = "inaccessible"
	badGIDGroup       = "badGID"

	correctUserId       = 10
	correctGroupId      = 20
	correctChildGroupId = 30
)

type (
	// Set of user IDs (map to nil values).
	userSet      = map[int]interface{}
	groupMembers = map[int]userSet
)

type fakeGitlaber struct {
	groupMembers groupMembers
}

type fakeGroup struct {
	name   string
	parent string
	id     int
}

func (g *fakeGroup) FullPath() string {
	if g.parent != "" {
		return fmt.Sprintf("%s/%s", g.parent, g.name)
	} else {
		return g.name
	}
}

func (g *fakeGroup) FullName() string {
	if g.parent != "" {
		return fmt.Sprintf("%s / %s", g.parent, g.name)
	} else {
		return g.name
	}
}

var fakeGroups = []fakeGroup{
	{name: correctGroup, id: correctGroupId},
	{name: correctChildGroup, parent: correctGroup, id: correctChildGroupId},
}

func (glc *fakeGitlaber) GetUsers(email string) ([]*ggl.User, error) {
	GinkgoWriter.Println("get users by email", email)
	if email == correctEmail {
		return []*ggl.User{{ID: correctUserId, Username: "Correct"}}, nil
	}
	if email == ambiguousEmail {
		return []*ggl.User{{Username: "Ambi"}, {Username: "Guous"}}, nil
	}
	return nil, &ggl.ErrorResponse{}
}

func (glc *fakeGitlaber) GetGroup(path string) (*ggl.Group, error) {
	GinkgoWriter.Println("get group", path)
	fooUrl, _ := url.Parse("http://gitlab/")
	fooRequest := http.Request{
		URL: fooUrl,
	}
	if path == inaccessibleGroup {
		return nil, &ggl.ErrorResponse{
			Response: &http.Response{
				StatusCode: http.StatusForbidden,
				Request:    &fooRequest,
			},
			Message: "group cannot be accessed",
		}
	}
	if path == badGIDGroup {
		return &ggl.Group{
			ID:       -1,
			Name:     badGIDGroup,
			FullName: badGIDGroup,
			FullPath: badGIDGroup,
		}, nil
	}

	for _, group := range fakeGroups {
		if path == group.FullPath() {
			return &ggl.Group{
				ID:       group.id,
				Name:     group.name,
				FullName: group.FullName(),
				FullPath: group.FullPath(),
			}, nil
		}
	}
	return nil, &ggl.ErrorResponse{
		Response: &http.Response{
			StatusCode: http.StatusNotFound,
			Request:    &fooRequest,
		},
		Message: gitlab.GroupNotFoundMsg,
	}
}

func (glc *fakeGitlaber) AddGroupMember(gid, uid int, access *ggl.AccessLevelValue) error {
	GinkgoWriter.Println("add", uid, "to group", gid)
	if gid == -1 || uid == -1 {
		return &ggl.ErrorResponse{
			Response: &http.Response{StatusCode: http.StatusBadRequest},
			Message:  "bad request",
		}
	}
	if users, ok := glc.groupMembers[gid]; ok {
		if _, ok := users[uid]; ok {
			return &ggl.ErrorResponse{
				Response: &http.Response{StatusCode: http.StatusConflict},
				Message:  gitlab.MemberAlreadyExistsMsg,
			}
		}

		users[uid] = nil
	} else {
		glc.groupMembers[gid] = userSet{uid: nil}
	}
	return nil
}

func (glc *fakeGitlaber) RemoveGroupMember(gid, uid int) error {
	GinkgoWriter.Println("remove", uid, "from group", gid)
	if users, ok := glc.groupMembers[gid]; ok {
		delete(users, uid)
	}
	// TODO: add tests for error handling
	return nil
}

func (glc *fakeGitlaber) GetUserGroups(uid int) ([]string, error) {
	GinkgoWriter.Println("get groups for user", uid)

	userGroups := []string{}
	for _, group := range fakeGroups {
		if users, ok := glc.groupMembers[group.id]; ok {
			if _, ok := users[uid]; ok {
				userGroups = append(userGroups, group.name)
			}
		}
	}
	// TODO: add tests for error handling
	return userGroups, nil
}

// The three functions below are not used in the GitLab backend test
func (glc *fakeGitlaber) GetUsersByUsername(username string) ([]*ggl.User, error) {
	GinkgoWriter.Println("get users by username", username)
	if username == "correct" {
		return []*ggl.User{{ID: correctUserId, Username: "Correct"}}, nil
	}
	return nil, &ggl.ErrorResponse{}
}

func (glc *fakeGitlaber) GetAllUsers() ([]string, error) {
	return nil, fmt.Errorf("Not implemented")
}

func (glc *fakeGitlaber) BlockUser(uid int) error {
	return fmt.Errorf("Not implemented")
}

func forEachGroupIt(description string, fn func(group *fakeGroup)) {
	for _, group := range fakeGroups {
		group := group
		It(fmt.Sprintf("%s [%s]", description, group.FullPath()), func() {
			fn(&group)
		})
	}
}

var _ = Describe("Gitlab", func() {
	var (
		gl  *gitlab.Gitlab
		glc *fakeGitlaber
		err error
	)

	BeforeEach(func() {
		glc = &fakeGitlaber{groupMembers: groupMembers{}}
		gl = &gitlab.Gitlab{glc}
	})

	Describe("Apply", func() {
		Context("when claims are nil", func() {
			It("should fail and return an error", func() {
				err = gl.Apply(nil, &config.Config{})
				Expect(err).To(HaveOccurred())
				Expect(err.Error()).To(Equal("no claims to apply"))
			})
		})

		Context("when there is no user for given email", func() {
			It("should fail and return an error", func() {
				err = gl.Apply(&claims.Claims{Email: ""}, &config.Config{
					Retries: 1,
				})
				Expect(err).To(HaveOccurred())
				Expect(err.Error()).To(ContainSubstring("failed to verify user availibility:"))
			})
		})

		Context("when there is no group for given membership claim", func() {
			It("should fail and return an error", func() {
				err = gl.Apply(&claims.Claims{
					Email: correctEmail,
					Username: correctUsername,
					Groups: []string{inaccessibleGroup},
				}, &config.Config{
					Retries: 1,
				})
				Expect(err).To(HaveOccurred())
				Expect(err.Error()).To(ContainSubstring("failed to get or create group:"))
			})
		})

		Context("when user cannot be added as a group member", func() {
			It("should fail and return an error", func() {
				err = gl.Apply(&claims.Claims{
					Email: correctEmail,
					Username: correctUsername,
					Groups: []string{badGIDGroup},
				}, &config.Config{
					Retries: 1,
				})
				Expect(err).To(HaveOccurred())
				Expect(err.Error()).To(ContainSubstring("failed to add user (Correct) to group (badGID):"))
			})
		})

		Context("when user can be added as a group member", func() {
			forEachGroupIt("should add the user to the group", func(group *fakeGroup) {
				err = gl.Apply(&claims.Claims{
					Email: correctEmail,
					Username: correctUsername,
					Groups: []string{group.name},
				}, &config.Config{
					Retries:           1,
					GitlabParentGroup: group.parent,
				})
				Expect(err).ToNot(HaveOccurred())
				Expect(glc.groupMembers).To(HaveLen(1))
				Expect(glc.groupMembers).To(HaveKey(group.id))
				Expect(glc.groupMembers[group.id]).To(HaveKey(correctUserId))
			})
		})

		Context("when user can be removed as a group member", func() {
			forEachGroupIt("should remove the user from the group", func(group *fakeGroup) {
				glc.groupMembers[group.id] = userSet{correctUserId: nil}
				err = gl.Apply(&claims.Claims{
					Email: correctEmail,
					Username: correctUsername,
					Groups: []string{},
				}, &config.Config{
					IncludedGroups:    []string{group.name},
					Retries:           1,
					GitlabParentGroup: group.parent,
				})
				Expect(err).ToNot(HaveOccurred())
				Expect(glc.groupMembers).To(HaveLen(1))
				Expect(glc.groupMembers).To(HaveKey(group.id))
				Expect(glc.groupMembers[group.id]).ToNot(HaveKey(correctUserId))
			})
		})

		Context("when user is already in the desired group", func() {
			forEachGroupIt("should leave the user in the group", func(group *fakeGroup) {
				glc.groupMembers[group.id] = userSet{correctUserId: nil}
				err = gl.Apply(&claims.Claims{
					Email: correctEmail,
					Username: correctUsername,
					Groups: []string{group.name},
				}, &config.Config{
					IncludedGroups:    []string{group.name},
					Retries:           1,
					GitlabParentGroup: group.parent,
				})
				Expect(err).ToNot(HaveOccurred())
				Expect(glc.groupMembers).To(HaveLen(1))
				Expect(glc.groupMembers).To(HaveKey(group.id))
				Expect(glc.groupMembers[group.id]).To(HaveKey(correctUserId))
			})
		})
	})

	Describe("GetUser", func() {
		var u *ggl.User

		Context("when requested email is invalid", func() {
			It("should fail and return an error", func() {
				u, err = gl.GetUser("")
				Expect(err).To(HaveOccurred())
				Expect(err.Error()).To(ContainSubstring("failed to get GitLab user:"))
				Expect(u).To(BeNil())
			})
		})

		Context("when requested email is ambiguous", func() {
			It("should fail and return an error", func() {
				u, err = gl.GetUser(ambiguousEmail)
				Expect(err).To(HaveOccurred())
				Expect(err.Error()).To(ContainSubstring("wrong number of GitLab users for given name:"))
				Expect(u).To(BeNil())
			})
		})

		Context("when requested email is correct", func() {
			It("should return Correct user", func() {
				u, err = gl.GetUser(correctEmail)
				Expect(err).ToNot(HaveOccurred())
				Expect(u).To(Equal(&ggl.User{ID: correctUserId, Username: "Correct"}))
			})
		})
	})

	Describe("AddMember", func() {
		Context("when arguments are incorrect", func() {
			It("should fail and return an error", func() {
				err = gl.AddMember(-1, -1, ggl.AccessLevel(ggl.NoPermissions))
				Expect(err).To(HaveOccurred())
				Expect(err.Error()).To(ContainSubstring("failed to add GitLab group member:"))
			})
		})

		Context("when member already exists", func() {
			It("should handle received error gracefully", func() {
				err = gl.AddMember(1, 1, ggl.AccessLevel(ggl.MaintainerPermissions))
				Expect(err).ToNot(HaveOccurred())
			})
		})

		Context("when member can be added", func() {
			It("should add a member", func() {
				err = gl.AddMember(correctUserId, correctGroupId, ggl.AccessLevel(ggl.OwnerPermissions))
				Expect(err).ToNot(HaveOccurred())
			})
		})
	})
})
