// Copyright (C) 2021, Paweł Wieczorek
// Copyright (C) 2021, Collabora Limited
//
// SPDX-License-Identifier: Apache-2.0

package gitlab

import (
	"fmt"
	"strings"

	"github.com/apex/log"
	"github.com/xanzy/go-gitlab"
)

// Gitlaber provides passthrough interface to the GitLab API.
// It does not handle any errors. It only ensures passed arguments comply with GitLab API requirements.
//
// Mocking this interface allows testing higher level logic without GitLab instance.
type Gitlaber interface {
	GetUsers(string) ([]*gitlab.User, error)
	GetUsersByUsername(username string) ([]*gitlab.User, error)
	GetGroup(string) (*gitlab.Group, error)
	AddGroupMember(int, int, *gitlab.AccessLevelValue) error
	RemoveGroupMember(gid, uid int) error
	GetUserGroups(uid int) ([]string, error)
	// Get all non-blocked users (returns usernames)
	GetAllUsers() ([]string, error)
	BlockUser(int) error
}

// gitlabClient implements Gitlaber interface.
// This structure contains only the data required to interact with GitLab instance.
type gitlabClient struct {
	client *gitlab.Client
}

func newGitlabClient(secret, url string) (*gitlabClient, error) {
	glc, err := gitlab.NewClient(secret, gitlab.WithBaseURL(url))
	if err != nil {
		return nil, fmt.Errorf("failed to create GitLab client: %w", err)
	}
	return &gitlabClient{glc}, nil
}

func (glc *gitlabClient) GetUsers(email string) ([]*gitlab.User, error) {
	user, _, err := glc.client.Users.ListUsers(&gitlab.ListUsersOptions{Search: gitlab.String(email)})
	return user, err
}

func (glc *gitlabClient) GetUsersByUsername(username string) ([]*gitlab.User, error) {
	users, _, err := glc.client.Users.ListUsers(&gitlab.ListUsersOptions{Username: gitlab.String(username)})
	return users, err
}

func (glc *gitlabClient) GetGroup(path string) (*gitlab.Group, error) {
	grp, _, err := glc.client.Groups.GetGroup(path, nil)
	return grp, err
}

func (glc *gitlabClient) AddGroupMember(gid, uid int, access *gitlab.AccessLevelValue) error {
	_, _, err := glc.client.GroupMembers.AddGroupMember(gid, &gitlab.AddGroupMemberOptions{UserID: &uid, AccessLevel: access})
	return err
}

func (glc *gitlabClient) RemoveGroupMember(gid, uid int) error {
	_, err := glc.client.GroupMembers.RemoveGroupMember(gid, uid, nil)
	return err
}

func (glc *gitlabClient) GetUserGroups(uid int) ([]string, error) {
	t := "Namespace"
	opt := &gitlab.GetUserMembershipOptions{
		Type: &t,
	}
	memberships, _, err := glc.client.Users.GetUserMemberships(uid, opt)
	if err != nil {
		return nil, err
	}

	groups := []string{}
	for _, m := range memberships {
		groups = append(groups, m.SourceName)
	}

	return groups, nil
}

func (glc *gitlabClient) GetAllUsers() ([]string, error) {
	opt := &gitlab.ListUsersOptions{
		ListOptions: gitlab.ListOptions{
			PerPage: 100,
			Page:    1,
		},
	}

	usernames := []string{}

	for {
		users, resp, err := glc.client.Users.ListUsers(opt)
		log.WithFields(log.Fields{
			"page": resp.CurrentPage,
			"total": resp.TotalPages,
		}).WithError(err).Debug("get_users")

		if err != nil {
			return nil, err
		}

		for _, u := range users {
			if !u.Bot && u.State != "blocked" {
				usernames = append(usernames, strings.ToLower(u.Username))
			}
		}

		if resp.CurrentPage >= resp.TotalPages {
			break
		}

		opt.Page = resp.NextPage
	}

	return usernames, nil
}

func (glc *gitlabClient) BlockUser(uid int) error {
	return glc.client.Users.BlockUser(uid, nil)
}
