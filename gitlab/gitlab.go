// Copyright (C) 2021,      Paweł Wieczorek
// Copyright (C) 2021-2022, Andrej Shadura
// Copyright (C)      2022, Ryan Gonzalez
// Copyright (C) 2021—2022, Collabora Limited
//
// SPDX-License-Identifier: Apache-2.0

package gitlab

import (
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/apex/log"
	mapset "github.com/deckarep/golang-set/v2"
	"github.com/xanzy/go-gitlab"

	. "gitlab.apertis.org/infrastructure/authz-mediator/claims"
	"gitlab.apertis.org/infrastructure/authz-mediator/config"
)

const (
	GroupNotFoundMsg       = "{message: 404 Group Not Found}"
	MemberAlreadyExistsMsg = "{message: Member already exists}"
)

// Gitlab handles service interaction logic.
// It provides only high level functions, handles possible errors and verifies which ones can be safely ignored.
// This structure contains no data to access GitLab instance nor methods to call its API directly.
type Gitlab struct {
	Gitlaber
}

func NewGitlab(secret, url string) (*Gitlab, error) {
	gl, err := newGitlabClient(secret, url)
	if err != nil {
		return nil, fmt.Errorf("failed to create gitlab backend: %w", err)
	}
	return &Gitlab{gl}, nil
}

// Apply maps received claims to group memberships for given service.
// Setting config.Retries to 0 disables check for asset availability on the service.
// Setting config.BackOff to 0 disables interval between subsequent checks. BackOff is expected to be in milliseconds.
func (gl *Gitlab) Apply(claims *Claims, config *config.Config) error {
	if claims == nil {
		return fmt.Errorf("no claims to apply")
	}

	err := retry(config.Retries, time.Duration(config.BackOff*int(time.Millisecond)), func() bool { return gl.userExists(claims.Email, claims.Username) })
	if err != nil {
		return fmt.Errorf("failed to verify user availibility: %w", err)
	}
	u, err := gl.FindUser(claims.Email, claims.Username)
	if err != nil {
		return fmt.Errorf("failed to get user: %w", err)
	}

	gitlabGroupPrefix := config.GitlabParentGroup
	if gitlabGroupPrefix != "" && !strings.HasSuffix(gitlabGroupPrefix, "/") {
		gitlabGroupPrefix += "/"
	}

	newGroups := mapset.NewSet(claims.Groups...)
	existingGroups := mapset.NewSet[string]()
	if groups, err := gl.GetUserGroups(u.ID); err == nil {
		groups = FilterGroups(groups, config.IncludedGroups, []string{})
		existingGroups = mapset.NewSet(groups...)
	}

	logctx := log.WithFields(log.Fields{
		"backend": "gitlab",
		"user":    u.Username,
		"user_id": u.ID,
	})

	to_add := newGroups.Difference(existingGroups)
	to_remove := existingGroups.Difference(newGroups)

	logctx.WithFields(log.Fields{
		"existing_groups": existingGroups,
		"to_add":		   to_add,
		"to_remove":	   to_remove,
	}).Debug("group membership changes")

	for gc := range to_add.Iter() {
		gc = gitlabGroupPrefix + gc
		grp, err := gl.GetGroup(gc)
		if err != nil {
			if !verifyGroupNotFound(err.(*gitlab.ErrorResponse)) {
				return fmt.Errorf("failed to get or create group: %w", err)
			} else {
				logctx.WithField("group", gc).WithError(err).Warn("non-existent group, not adding")
				continue
			}
		}

		logctx.WithField("group", grp.FullName).Info("adding user")
		err = gl.AddMember(grp.ID, u.ID, gitlab.AccessLevel(gitlab.MaintainerPermissions))
		if err != nil {
			return fmt.Errorf("failed to add user (%s) to group (%s): %w", u.Username, grp.FullName, err)
		}
	}

	for gc := range to_remove.Iter() {
		gc = gitlabGroupPrefix + gc
		grp, err := gl.GetGroup(gc)
		if err != nil {
			if !verifyGroupNotFound(err.(*gitlab.ErrorResponse)) {
				return fmt.Errorf("failed to get GitLab group: %w", err)
			}
		} else {
			logctx.WithField("group", grp.FullName).Info("removing user")
			err = gl.RemoveGroupMember(grp.ID, u.ID)
			if err != nil {
				return fmt.Errorf("failed to remove user %s from group %s: %w", u.Username, grp.FullName, err)
			}
		}
	}

	return nil
}

func (gl *Gitlab) FindUser(email string, username string) (*gitlab.User, error) {
	// Azure sets nickname as <userid>@<domain>
	usernameParts := strings.Split(username, "@")
	user, err := gl.GetUserByUsername(usernameParts[0])

	if user != nil {
		return user, nil
	}

	return nil, fmt.Errorf("failed to get GitLab user: %w", err)
}

func (gl *Gitlab) GetUser(email string) (*gitlab.User, error) {
	user, err := gl.GetUsers(email)
	if err != nil {
		return nil, fmt.Errorf("failed to get GitLab user: %s", err.(*gitlab.ErrorResponse).Message)
	}
	if len(user) != 1 {
		return nil, fmt.Errorf("wrong number of GitLab users for given name: expected %d, got %d", 1, len(user))
	}
	return user[0], nil
}

func (gl *Gitlab) GetUserByUsername(email string) (*gitlab.User, error) {
	user, err := gl.GetUsersByUsername(email)
	if err != nil {
		return nil, fmt.Errorf("failed to get GitLab user: %s", err.(*gitlab.ErrorResponse).Message)
	}
	if len(user) != 1 {
		return nil, fmt.Errorf("wrong number of GitLab users for given name: expected %d, got %d", 1, len(user))
	}
	return user[0], nil
}

func (gl *Gitlab) AddMember(gid, uid int, access *gitlab.AccessLevelValue) error {
	err := gl.AddGroupMember(gid, uid, access)
	if err != nil && !verifyMemberAlreadyExists(err.(*gitlab.ErrorResponse)) {
		return fmt.Errorf("failed to add GitLab group member: %w", err)
	}
	return nil
}

func (gl *Gitlab) LockUser(user string) error {
	uu, err := gl.GetUsersByUsername(user)
	if err != nil {
		return fmt.Errorf("failed to get GitLab user: %s", err.(*gitlab.ErrorResponse).Message)
	}
	for _, u := range uu {
		e := gl.BlockUser(u.ID)
		if e != nil {
			err = e
		}
	}

	return err
}

func (gl *Gitlab) userExists(email string, username string) bool {
	_, err := gl.FindUser(email, username)
	return err == nil
}

func retry(attempts int, sleep time.Duration, f func() bool) error {
	if attempts < 1 {
		return fmt.Errorf("requested %d attempts, nothing to do", attempts)
	}
	for i := 0; i < attempts; i++ {
		if ok := f(); ok {
			return nil
		}
		time.Sleep(sleep)
	}
	return fmt.Errorf("failed after %d attempts", attempts)
}

func verifyGroupNotFound(eresp *gitlab.ErrorResponse) bool {
	if eresp.Response.StatusCode != http.StatusNotFound {
		return false
	}
	if eresp.Message != GroupNotFoundMsg {
		return false
	}
	return true
}

func verifyMemberAlreadyExists(eresp *gitlab.ErrorResponse) bool {
	if eresp.Response.StatusCode != http.StatusConflict {
		return false
	}
	if eresp.Message != MemberAlreadyExistsMsg {
		return false
	}
	return true
}
