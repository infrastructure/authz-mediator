package gitlab

import (
	"fmt"
	"net/http"

	ggl "github.com/xanzy/go-gitlab"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

var _ = Describe("Internal", func() {
	var (
		eresp  *ggl.ErrorResponse
		result bool
	)

	Describe("verifyGroupNotFound", func() {
		Context("when error response has unexpected status code", func() {
			It("should return false", func() {
				eresp = &ggl.ErrorResponse{
					Response: &http.Response{StatusCode: http.StatusBadRequest},
				}
				result = verifyGroupNotFound(eresp)
				Expect(result).To(BeFalse())
			})
		})

		Context("when error response contains unexpected message", func() {
			It("should return false", func() {
				eresp = &ggl.ErrorResponse{
					Response: &http.Response{StatusCode: http.StatusNotFound},
					Message:  "foo",
				}
				result = verifyGroupNotFound(eresp)
				Expect(result).To(BeFalse())
			})
		})

		Context("when error response is the Group Not Found error", func() {
			It("should return true", func() {
				eresp = &ggl.ErrorResponse{
					Response: &http.Response{StatusCode: http.StatusNotFound},
					Message:  GroupNotFoundMsg,
				}
				result = verifyGroupNotFound(eresp)
				Expect(result).To(BeTrue())
			})
		})
	})

	Describe("verifyGroupNotFound", func() {
		Context("when error response has unexpected status code", func() {
			It("should return false", func() {
				eresp = &ggl.ErrorResponse{
					Response: &http.Response{StatusCode: http.StatusBadRequest},
				}
				result = verifyMemberAlreadyExists(eresp)
				Expect(result).To(BeFalse())
			})
		})

		Context("when error response contains unexpected message", func() {
			It("should return false", func() {
				eresp = &ggl.ErrorResponse{
					Response: &http.Response{StatusCode: http.StatusConflict},
					Message:  "foo",
				}
				result = verifyMemberAlreadyExists(eresp)
				Expect(result).To(BeFalse())
			})
		})

		Context("when error response is the Member Already Exists error", func() {
			It("should return true", func() {
				eresp = &ggl.ErrorResponse{
					Response: &http.Response{StatusCode: http.StatusConflict},
					Message:  MemberAlreadyExistsMsg,
				}
				result = verifyMemberAlreadyExists(eresp)
				Expect(result).To(BeTrue())
			})
		})
	})

	Describe("retry", func() {
		var (
			truer  func() bool
			falser func() bool
		)

		BeforeEach(func() {
			truer = func() bool { return true }
			falser = func() bool { return false }
		})

		Context("when attempts number is malformed", func() {
			It("should fail immediately", func() {
				attempts := -1
				err := retry(attempts, 0, falser)
				Expect(err).To(HaveOccurred())
				Expect(err.Error()).To(Equal(fmt.Sprintf("requested %d attempts, nothing to do", attempts)))
			})
		})

		Context("when sleep time is malformed or disabled", func() {
			It("should not affect retrying but return immediately", func() {
				attempts := 1
				err := retry(attempts, -1, truer)
				Expect(err).ToNot(HaveOccurred())
			})
		})

		Context("when function never succeeds", func() {
			It("should fail after all attmepts", func() {
				attempts := 3
				err := retry(attempts, 0, falser)
				Expect(err).To(HaveOccurred())
				Expect(err.Error()).To(Equal(fmt.Sprintf("failed after %d attempts", attempts)))
			})
		})

		Context("when function succeeds", func() {
			It("should complete without an error", func() {
				attempts := 1
				err := retry(attempts, 0, truer)
				Expect(err).To(BeNil())
			})
		})
	})
})
