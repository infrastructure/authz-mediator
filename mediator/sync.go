// Copyright (C) 2022, Andrej Shadura
// Copyright (C) 2022, Collabora Limited
//
// SPDX-License-Identifier: Apache-2.0

package mediator

import (
	"github.com/apex/log"
	mapset "github.com/deckarep/golang-set/v2"

	 . "gitlab.apertis.org/infrastructure/authz-mediator/claims"
)

type membershipCache map[string]mapset.Set[string]

// Enforce group membership as reported by the authentication service (MS Graph API)
// on the backend (GitLab, OBS, ...) by locking users who are not members of the
// groups on the authentication service.
// Newly added users are not added automatically, since that will happen when
// they log in for the first time.
func (med *Mediator) EnforceGroups() {
	if med.backend == nil {
		return
	}

	if len(med.config.RequiredGroups) < 1 {
		return
	}

	gg, err := med.groupSource.GroupsMatching(med.config.RequiredGroups)
	log.WithError(err).
		WithField("result", gg).
		Debug("groups to sync")

	if err != nil {
		return
	}

	if med.groupDataCache == nil {
		med.groupDataCache = membershipCache{}
	}

	for _, g := range gg {
		uu, err := med.groupSource.GroupMembers(g.ID)
		log.WithError(err).
			WithField("g", g).
			WithField("result", uu).
			Debug("source group members")

		if err != nil {
			return
		}

		old, haveCache := med.groupDataCache[g.ID]

		med.groupDataCache[g.ID] = mapset.NewSet[string]()

		for username := range uu {
			localPart := UsernameLocalPart(username)
			med.groupDataCache[g.ID].Add(localPart)
		}

		if haveCache {
			removed := old.Difference(med.groupDataCache[g.ID])

			// TODO: We can use this to optimise user removal in future
			// Right now it’s only used to ease debugging
			log.WithField("members", removed).Debug("removed members")
		}

		backendMembers, err := med.backend.GetAllUsers()

		if err != nil {
			log.WithError(err).Error("backend users")
			return
		}

		backendMemberSet := mapset.NewSet[string](backendMembers...)
		log.WithError(err).
			WithField("result", backendMemberSet).
			Debug("backend members")

		backendMemberSet.Remove(med.config.MediatorUsername)

		diff := backendMemberSet.Difference(med.groupDataCache[g.ID])

		ignoredSet := mapset.NewSet[string](med.config.EnforceGroupsIgnoreUsers...)
		diff = diff.Difference(ignoredSet)

		log.WithFields(log.Fields{
			"members": diff,
			"size":    diff.Cardinality(),
		}).Info("users to be blocked")

		// We don’t directly need this, but it’s useful for debugging
		rdiff := med.groupDataCache[g.ID].Difference(backendMemberSet)
		log.WithFields(log.Fields{
			"members": rdiff,
			"size":    rdiff.Cardinality(),
		}).Debug("users not registered on backend")

		if med.config.EnforceGroupsAuditOnly {
			log.Info("group enforcement in audit-only mode, not actually locking users")
			return
		}

		for username := range diff.Iter() {
			logctx := log.WithField("username", username).
						  Trace("locking user")

			err := med.backend.LockUser(username)
			logctx.Stop(&err)
		}
	}
}
