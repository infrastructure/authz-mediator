// Copyright (C) 2021, Paweł Wieczorek
// Copyright (C) 2021, Collabora Limited
//
// SPDX-License-Identifier: Apache-2.0

package mediator

import (
	"context"
	"fmt"
	"net/http"

	"github.com/coreos/go-oidc/v3/oidc"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"gitlab.apertis.org/infrastructure/authz-mediator/config"
	"gitlab.apertis.org/infrastructure/authz-mediator/endpoints"
	"gitlab.apertis.org/infrastructure/authz-mediator/membership"
)

type FakeVerifier struct{}

func (f FakeVerifier) Verify(ctx context.Context, rawIDToken string) (*oidc.IDToken, error) {
	return &oidc.IDToken{}, nil
}

func NewTestMediator(mediatorSecret string, gs membership.GroupSource, b MediatorBackend, verifier MediatorVerifier, e *endpoints.Endpoints, c *config.Config) *Mediator {
	return &Mediator{
		secret:      mediatorSecret,
		endpoints:   e,
		config:      c,
		client:      http.DefaultClient,
		verifier:    verifier,
		backend:     b,
		groupSource: gs,
	}
}

var _ = Describe("Internal", func() {
	Describe("oauth2Config", func() {
		var (
			scope []string

			secret string
			client *http.Client
			v      MediatorVerifier
			e      *endpoints.Endpoints
			c      *config.Config
		)

		BeforeEach(func() {
			scope = []string{"foo", "bar", "baz"}

			secret = "quuz"
			client = http.DefaultClient
			v = &FakeVerifier{}
			e = &endpoints.Endpoints{}
			c = &config.Config{Redirect: "https://foo", Backend: GitLabBackend}
		})

		Context("when groups scope is disabled", func() {
			It("should not include group scope in oauth2 config", func() {
				c.GroupsScope = false
				med, err := NewMediator(secret, secret, client, v, e, c)
				Expect(err).ToNot(HaveOccurred())

				o2c := med.oauth2Config(scope)
				Expect(o2c.Scopes).ToNot(ContainElement(GroupsScope))
				Expect(o2c.Scopes).To(Equal(scope))
			})
		})

		Context("when groups scope is enabled", func() {
			It("should include group scope in oauth2 config", func() {
				c.GroupsScope = true
				med, err := NewMediator(secret, secret, client, v, e, c)
				Expect(err).ToNot(HaveOccurred())

				o2c := med.oauth2Config(scope)
				Expect(o2c.Scopes).To(ContainElement(GroupsScope))
				Expect(o2c.Scopes).To(Equal(append(scope, GroupsScope)))
			})
		})
	})

	Describe("serviceURL", func() {
		var (
			service string
			err     error
		)

		Context("when input is not parsable", func() {
			BeforeEach(func() {
				service, err = serviceURL("\r\n")
			})

			It("should return an empty string and an error", func() {
				Expect(err).To(HaveOccurred())
				Expect(err.Error()).To(ContainSubstring("failed to parse URL:"))
				Expect(service).To(BeEmpty())
			})
		})

		Context("when proper input is used", func() {
			const (
				scheme = "scheme"
				host   = "host:73313" // port is matched against /^:\d*$/
			)

			BeforeEach(func() {
				service, err = serviceURL(fmt.Sprintf("%s://userinfo@%s/path?query#fragment", scheme, host))
			})

			It("should return a string containing scheme and host and no error", func() {
				Expect(err).ToNot(HaveOccurred())
				Expect(service).To(Equal(fmt.Sprintf("%s://%s", scheme, host)))
			})
		})
	})
})
