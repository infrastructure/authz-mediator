package mediator_test

import (
	"testing"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

func TestMediator(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Mediator Suite")
}
