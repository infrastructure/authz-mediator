// Copyright (C) 2021, Paweł Wieczorek
// Copyright (C) 2021, Collabora Limited
//
// SPDX-License-Identifier: Apache-2.0

package mediator

import (
	"context"
	"fmt"
	"net/http"
	"net/url"
	"os"

	"github.com/apex/log"
	"github.com/coreos/go-oidc/v3/oidc"

	"gitlab.apertis.org/infrastructure/authz-mediator/claims"
	"gitlab.apertis.org/infrastructure/authz-mediator/config"
	"gitlab.apertis.org/infrastructure/authz-mediator/endpoints"
	"gitlab.apertis.org/infrastructure/authz-mediator/gitlab"
	"gitlab.apertis.org/infrastructure/authz-mediator/lava"
	"gitlab.apertis.org/infrastructure/authz-mediator/membership"
	"gitlab.apertis.org/infrastructure/authz-mediator/obs"
)

const (
	GitLabBackend = "gitlab"
	OBSBackend    = "obs"
	LAVABackend   = "lava"
	NoneBackend   = "none"
)

type Mediator struct {
	secret    string
	endpoints *endpoints.Endpoints
	config    *config.Config

	client   *http.Client
	verifier MediatorVerifier

	backend MediatorBackend
	groupSource    membership.GroupSource
	groupDataCache membershipCache
}

type MediatorVerifier interface {
	Verify(ctx context.Context, rawIDToken string) (*oidc.IDToken, error)
}

type MediatorBackend interface {
	Apply(claims *claims.Claims, config *config.Config) error
	GetAllUsers() ([]string, error)
	LockUser(string) error
}

func NewMediator(mediatorSecret, backendSecret string, client *http.Client, verifier MediatorVerifier, e *endpoints.Endpoints, c *config.Config) (*Mediator, error) {
	var (
		backend MediatorBackend
		err     error
	)
	switch c.Backend {
	case GitLabBackend:
		gitlabURL, err := serviceURL(c.Redirect)
		if err != nil {
			return nil, fmt.Errorf("failed to parse redirect URL: %w", err)
		}
		backend, err = gitlab.NewGitlab(backendSecret, gitlabURL)
		if err != nil {
			return nil, fmt.Errorf("failed to create gitlab: %w", err)
		}
	case OBSBackend:
		obsURL, err := serviceURL(c.Redirect)
		if err != nil {
			return nil, fmt.Errorf("failed to parse redirect URL: %w", err)
		}
		backend, err = obs.NewOBS(backendSecret, obsURL)
		if err != nil {
			return nil, fmt.Errorf("failed to create obs: %w", err)
		}
	case LAVABackend:
		backend, err = lava.NewLAVA(backendSecret, c.Redirect)
		if err != nil {
			return nil, fmt.Errorf("failed to create lava: %w", err)
		}
	case NoneBackend:
		backend = nil
	default:
		return nil, fmt.Errorf("requested backend is not supported: %s", c.Backend)
	}

	var groupSource membership.GroupSource

	if c.EnforceGroups {
		azureSecret := mediatorSecret
		if use_az_cli, ok := os.LookupEnv("AUTHZ_MEDIATOR_USE_AZ_CLI"); ok {
			if config.Truthy(use_az_cli) {
				log.Warn("Using az cli to talk to Azure!")
				azureSecret = ""
			}
		}

		groupSource, err = membership.NewAzureGroupSource(c, azureSecret)
		if err != nil {
			return nil, err
		}
	} else {
		groupSource = nil
	}

	return &Mediator{
		secret:      mediatorSecret,
		endpoints:   e,
		config:      c,
		client:      client,
		verifier:    verifier,
		backend:     backend,
		groupSource: groupSource,
	}, nil
}

func serviceURL(input string) (string, error) {
	service, err := url.Parse(input)
	if err != nil {
		return "", fmt.Errorf("failed to parse URL: %w", err)
	}
	return fmt.Sprintf("%s://%s", service.Scheme, service.Host), nil
}
