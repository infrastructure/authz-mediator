// Copyright (C) 2021, Paweł Wieczorek
// Copyright (C) 2021, Andrej Shadura
// Copyright (C) 2021, Collabora Limited
//
// SPDX-License-Identifier: Apache-2.0

package mediator

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/apex/log"
	"github.com/coreos/go-oidc/v3/oidc"
	"golang.org/x/oauth2"

	. "gitlab.apertis.org/infrastructure/authz-mediator/claims"
)

const (
	AuthRoute     = "/auth"
	TokenRoute    = "/token"
	PubKeysRoute  = "/keys"
	UserInfoRoute = "/userinfo"
	CallbackRoute = "/callback"

	StateParam = "state"
	NonceParam = "nonce"
	ScopeParam = "scope"
	CodeParam  = "code"

	ClientIDParam     = "client_id"
	ClientSecretParam = "client_secret"

	GroupsScope = "groups"
)

func (med *Mediator) AuthHandler(w http.ResponseWriter, r *http.Request) {
	requestURI, err := url.ParseRequestURI(r.RequestURI)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// From this point on, all responses (including errors) are known to be JSON
	w.Header().Set("content-type", "application/json")

	query := requestURI.Query()
	state := query.Get(StateParam)
	if state == "" {
		http.Error(w, formatJsonError("invalid_request", "missing %s param", StateParam), http.StatusBadRequest)
		return
	}
	nonce := query.Get(NonceParam)
	if nonce == "" {
		http.Error(w, formatJsonError("invalid_request", "missing %s param", NonceParam), http.StatusBadRequest)
		return
	}
	scope := query.Get(ScopeParam)
	if scope == "" {
		http.Error(w, formatJsonError("invalid_request", "missing %s param", ScopeParam), http.StatusBadRequest)
		return
	}
	clientID := query.Get(ClientIDParam)
	if clientID == "" {
		http.Error(w, formatJsonError("invalid_request", "missing %s param", ClientIDParam), http.StatusBadRequest)
		return
	}

	if clientID != med.config.ID {
		http.Error(w, formatJsonError("invalid_client", "invalid %s param", ClientIDParam), http.StatusBadRequest)
		return
	}

	redirectURL := med.oauth2Config(strings.Split(scope, " ")).AuthCodeURL(state, oauth2.SetAuthURLParam(NonceParam, nonce))
	log.WithField("redirect_url", redirectURL).Debug("auth")
	http.Redirect(w, r, redirectURL, http.StatusSeeOther)
}

func (med *Mediator) TokenHandler(w http.ResponseWriter, r *http.Request) {
	// From this point on, all responses (including errors) are known to be JSON
	w.Header().Set("content-type", "application/json; charset=utf-8")

	code := r.FormValue(CodeParam)
	if code == "" {
		http.Error(w, formatJsonError("invalid_request", "missing %s param", CodeParam), http.StatusBadRequest)
		return
	}

	clientID, clientSecret, ok := r.BasicAuth()
	if !ok {
		clientID = r.FormValue(ClientIDParam)
		if clientID == "" {
			http.Error(w, formatJsonError("invalid_request", "missing %s param", ClientIDParam), http.StatusBadRequest)
			return
		}
		clientSecret = r.FormValue(ClientSecretParam)
		if clientSecret == "" {
			http.Error(w, formatJsonError("invalid_request", "missing %s param", ClientSecretParam), http.StatusBadRequest)
			return
		}
	}

	if clientID != med.config.ID {
		http.Error(w, formatJsonError("invalid_client", "invalid %s param", ClientIDParam), http.StatusBadRequest)
		return
	}
	if clientSecret != med.secret {
		http.Error(w, formatJsonError("invalid_client", "invalid %s param", ClientSecretParam), http.StatusBadRequest)
		return
	}

	token, err := med.exchange(r.Context(), code)
	if err != nil {
		if rErr, ok := err.(*oauth2.RetrieveError); ok {
			log.WithFields(parseOAuthError(rErr)).Error("exchange")
			http.Error(w, string(rErr.Body), rErr.Response.StatusCode)
		} else {
			http.Error(w, formatJsonError("server_error", "failed to exchange code for token: %v", err), http.StatusInternalServerError)
		}
		return
	}

	idToken, err := med.verifier.Verify(r.Context(), token.IDToken)
	if err != nil {
		http.Error(w, formatJsonError("server_error", "failed to verify token: %v", err), http.StatusInternalServerError)
		return
	}

	claims, err := Extract(idToken)

	// nothing to apply if claims extraction failed
	if err == nil {
		logctx := log.WithFields(log.Fields{
			"username": claims.Username,
			"name":     claims.Name,
			"groups":   claims.Groups,
		})

		// First lower-case all groups for consistent handling throughout
		claims.Groups = LowerGroups(claims.Groups)

		// The reason mapping is done before filtering is that depending on the settings of
		// the auth service, groups may need to be mapped into sane names before being used
		// (e.g. if they come as UUIDs, not names).

		// only perform the mapping if a group map is specified
		if len(med.config.GroupMap) > 0 {
			claims.Groups = MapGroups(claims.Groups, med.config.GroupMap)
		}

		if len(med.config.RequiredGroups) > 0 {
			if !VerifyGroups(claims.Groups, med.config.RequiredGroups) {
				logctx.WithField("required_groups", med.config.RequiredGroups).Error("unauthorised")
				http.Error(w, formatJsonError("invalid_grant", "User not authorised to access this resource"), http.StatusBadRequest)
				return
			}
		}

		claims.Groups = FilterGroups(claims.Groups, med.config.IncludedGroups, []string{})

		logctx = logctx.WithField("mapped_groups", claims.Groups)

		if med.backend == nil {
			logctx.Debug("authorised, but no membership information")
		} else {
			logctx.Trace("authorised, applying membership information")

			go func() {
				err := med.backend.Apply(claims, med.config) // membership mapping should not affect tokens relay
				logctx.Stop(&err)
			}()
		}
	}

	jsonToken, err := json.Marshal(token)
	if err != nil {
		http.Error(w, formatJsonError("server_error", "failed to marshal token: %v", err), http.StatusInternalServerError)
		return
	}
	fmt.Fprintf(w, "%s", jsonToken)
}

func (med *Mediator) PubKeysHandler(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, med.endpoints.PubKeys, http.StatusSeeOther)
}

func (med *Mediator) UserInfoHandler(w http.ResponseWriter, r *http.Request) {
	req, err := http.NewRequest(http.MethodGet, med.endpoints.UserInfo, nil)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	req.Header = r.Header
	req.Header.Del("accept-encoding")

	resp, err := med.client.Do(req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("content-type", "application/json; charset=utf-8")
	fmt.Fprintf(w, "%s", body)
}

func (med *Mediator) CallbackHandler(w http.ResponseWriter, r *http.Request) {
	requestURI, err := url.ParseRequestURI(r.RequestURI)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	redirectURL, err := url.Parse(med.config.Redirect)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	redirectURL.RawQuery = requestURI.Query().Encode()
	http.Redirect(w, r, redirectURL.String(), http.StatusSeeOther)
}

func (med *Mediator) MetadataHandler(w http.ResponseWriter, r *http.Request) {
	endpoints := *med.endpoints
	endpoints.Issuer = med.config.Mediator
	endpoints.Auth = med.config.Mediator + AuthRoute
	endpoints.Token = med.config.Mediator + TokenRoute
	endpoints.PubKeys = med.config.Mediator + PubKeysRoute
	endpoints.UserInfo = med.config.Mediator + UserInfoRoute
	jsonMetadata, err := json.Marshal(endpoints)
	if err != nil {
		http.Error(w, fmt.Sprintf("failed to marshal metadata: %v", err), http.StatusInternalServerError)
		return
	}
	w.Header().Set("content-type", "application/json; charset=utf-8")
	fmt.Fprintf(w, "%s", jsonMetadata)
}

func (med *Mediator) oauth2Config(scope []string) *oauth2.Config {
	if med.config.GroupsScope {
		scope = append(scope, GroupsScope)
	}
	return &oauth2.Config{
		ClientID:     med.config.ID,
		ClientSecret: med.secret,
		RedirectURL:  med.config.Mediator + CallbackRoute,
		Endpoint: oauth2.Endpoint{
			AuthURL:   med.endpoints.Auth,
			TokenURL:  med.endpoints.Token,
			AuthStyle: 0,
		},
		Scopes: scope,
	}
}

func (med *Mediator) exchange(parentCtx context.Context, code string) (*TokenResponse, error) {
	ctx := oidc.ClientContext(parentCtx, med.client)
	oauth2Config := med.oauth2Config([]string{})

	token, err := oauth2Config.Exchange(ctx, code)
	if err != nil {
		return nil, err
	}

	rawIDToken, ok := token.Extra("id_token").(string)
	if !ok {
		return nil, fmt.Errorf("no id_token in token response")
	}

	return toTokenResponse(rawIDToken, token.AccessToken, token.Expiry), nil
}

type TokenResponse struct {
	TokenType   string `json:"token_type"`
	AccessToken string `json:"access_token"`
	IDToken     string `json:"id_token"`
	ExpiresIn   int    `json:"expires_in"`
}

func toTokenResponse(idToken, accessToken string, expiry time.Time) *TokenResponse {
	return &TokenResponse{
		"bearer",
		accessToken,
		idToken,
		int(time.Until(expiry).Seconds()),
	}
}
