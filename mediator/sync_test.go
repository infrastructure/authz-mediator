// Copyright (C) 2022, Andrej Shadura
// Copyright (C) 2022, Collabora Limited
//
// SPDX-License-Identifier: Apache-2.0

package mediator_test

import (
	"fmt"

	"github.com/apex/log"
	"github.com/apex/log/handlers/text"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	. "github.com/onsi/gomega/gstruct"

	"gitlab.apertis.org/infrastructure/authz-mediator/claims"
	"gitlab.apertis.org/infrastructure/authz-mediator/config"
	"gitlab.apertis.org/infrastructure/authz-mediator/endpoints"
	"gitlab.apertis.org/infrastructure/authz-mediator/mediator"
	"gitlab.apertis.org/infrastructure/authz-mediator/membership"
)

type fakeUser struct {
	Locked bool
}

type fakeBackend struct {
	userDB map[string]fakeUser
}

func (fb *fakeBackend) Apply(*claims.Claims, *config.Config) error {
	return fmt.Errorf("Not implemented")
}

func (fb *fakeBackend) GetAllUsers() ([]string, error) {
	users := []string{}

	for username, user := range fb.userDB {
		if !user.Locked {
			users = append(users, username)
		}
	}
	return users, nil
}

func (fb *fakeBackend) LockUser(username string) error {
	if user, ok := fb.userDB[username]; ok {
		user.Locked = true
		fb.userDB[username] = user
		return nil
	} else {
		return fmt.Errorf("No user %s", username)
	}
}

type fakeGroupSource struct {
	groupMembers []string
}

func (fgs *fakeGroupSource) Groups() ([]membership.Member, error) {
	return []membership.Member{
		membership.Member{
			DisplayName: "Group",
			Name:        "group",
			Email:       "group@group.org",
			ID:          "group-id",
		},
	}, nil
}

func (fgs *fakeGroupSource) GroupsMatching(patterns []string) ([]membership.Member, error) {
	return fgs.Groups()
}

func (fgs *fakeGroupSource) GroupMembers(groupId string) (map[string]string, error) {
	if groupId == "group-id" {
		members := map[string]string{}
		for _, m := range fgs.groupMembers {
			members[m] = m + "@domain"
		}
		return members, nil
	} else {
		return nil, fmt.Errorf("No group %s", groupId)
	}
}

var allUsers = []string{
	"user1",
	"user2",
	"user3",
	"user4",
	"user5",
	"user6",
	"user7",
	"user8",
	"user9",
}

var _ = Describe("Mediator", func() {
	log.SetHandler(text.New(GinkgoWriter))
	log.SetLevel(log.DebugLevel)

	const (
		mediatorID  = "glhf"
		mediatorURL = "https://apert.is:1337"
		providerURL = "https://oidc.ftw"
		redirectURL = "https://phone.home:82526"
		notSecret   = "n0ts3kr3t"
	)

	var (
		med *mediator.Mediator
		b   *fakeBackend
		gs  *fakeGroupSource
	)

	BeforeEach(func() {
		e := &endpoints.Endpoints{
			Auth:     providerURL + mediator.AuthRoute,
			Token:    providerURL + mediator.TokenRoute,
			PubKeys:  providerURL + mediator.PubKeysRoute,
			UserInfo: providerURL + mediator.UserInfoRoute,
		}
		b = &fakeBackend{
			userDB: map[string]fakeUser{},
		}
		for _, u := range allUsers {
			b.userDB[u] = fakeUser{}
		}

		gs = &fakeGroupSource{}
		c := &config.Config{
			ID:       mediatorID,
			Backend:  "test",
			Provider: providerURL,
			Mediator: mediatorURL,
			Redirect: redirectURL,
		}
		c.RequiredGroups = []string{"group"}
		c.EnforceGroups = true
		c.EnforceGroupsIgnoreUsers = []string{"user9"}
		med = mediator.NewTestMediator(notSecret, gs, b, mediator.FakeVerifier{}, e, c)
	})

	Describe("Enforcement", func() {
		When("users are missing from the group", func() {
			It("should lock them", func() {
				// Only allow the first five users
				gs.groupMembers = allUsers[0:5]
				med.EnforceGroups()

				Expect(b.userDB).To(MatchKeys(IgnoreExtras, Keys{
					// These users should be blocked
					"user6": MatchAllFields(Fields{
						"Locked": BeTrue(),
					}),
					"user7": MatchAllFields(Fields{
						"Locked": BeTrue(),
					}),
					"user8": MatchAllFields(Fields{
						"Locked": BeTrue(),
					}),

					// This user should be kept active
					"user5": MatchAllFields(Fields{
						"Locked": BeFalse(),
					}),

					// This user should have been skipped
					"user9": MatchAllFields(Fields{
						"Locked": BeFalse(),
					}),
				}))
			})
		})

		When("another user is removed from the group", func() {
			It("should lock it too", func() {
				// Only allow the first five users
				gs.groupMembers = allUsers[0:5]
				med.EnforceGroups()

				// Now let’s remove another user
				gs.groupMembers = allUsers[0:4]
				med.EnforceGroups()

				Expect(b.userDB).To(MatchKeys(IgnoreExtras, Keys{
					// This user should be blocked
					"user5": MatchAllFields(Fields{
						"Locked": BeTrue(),
					}),

					// This user should be kept active
					"user4": MatchAllFields(Fields{
						"Locked": BeFalse(),
					}),
				}))
			})
		})
	})

})
