package mediator

import (
	"encoding/json"
	"fmt"

	"github.com/apex/log"
	"golang.org/x/oauth2"
)

type OIDCError struct {
	ErrorType        string `json:"error"`
	ErrorDescription string `json:"error_description,omitempty"`
	ErrorCodes       []int  `json:"error_codes,omitempty"`
	Timestamp        string `json:"timestamp,omitempty"`
	TraceId          string `json:"trace_id,omitempty"`
	CorrelationId    string `json:"correlation_id,omitempty"`
}

func (e *OIDCError) Error() string {
	return e.ErrorDescription
}

func (e *OIDCError) Fields() log.Fields {
	return log.Fields{
		"error": e.ErrorType,
	}
}

// TODO: This should probably parse into OIDCError, and then we can log and return it uniformly
func parseOAuthError(rErr *oauth2.RetrieveError) log.Fields {
	body := rErr.Body
	result := log.Fields{}
	if err := json.Unmarshal(body, &result); err != nil {
		result["error"] = "unknown_error"
		result["error_description"] = string(body)
	}
	return result
}

func formatJsonError(errorType, errorDescriptionFormat string, a ...interface{}) string {
	errorDescription := fmt.Sprintf(errorDescriptionFormat, a...)
	resp := OIDCError{
		ErrorType:        errorType,
		ErrorDescription: errorDescription,
	}
	log.WithError(&resp).Error(errorDescription)
	if jsonResp, err := json.Marshal(resp); err != nil {
		return errorDescription
	} else {
		return string(jsonResp)
	}
}
