FROM golang:1.22.4-alpine3.19 AS builder

WORKDIR /usr/local/src/authz-mediator

COPY go.mod go.sum ./
RUN go mod download

COPY . .

RUN go install

FROM alpine:3.19

COPY --from=builder /go/bin/authz-mediator /usr/local/bin/authz-mediator

USER 1001:1001

CMD ["authz-mediator"]
