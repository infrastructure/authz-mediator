import time
import urllib.parse

import pytest
from gitlab.exceptions import GitlabGetError
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


def _wait_for_page_load(driver, timeout):
    js = "return document.readyState === 'complete'"
    WebDriverWait(driver, timeout).until(lambda x: x.execute_script(js))


def _wait_for_url_change(driver, timeout, old_url):
    WebDriverWait(driver, timeout).until(EC.url_changes(old_url))


def _oidc_login(driver, timeout, url, user, passwd):
    driver.get(url)
    login_url = driver.current_url
    test_idp_button = driver.find_element(
        By.CSS_SELECTOR, "[data-testid='oidc-login-button']"
    )
    test_idp_button.click()
    _wait_for_url_change(driver, timeout, old_url=login_url)
    _wait_for_page_load(driver, timeout)
    idp_url = driver.current_url
    form = driver.find_element(By.CSS_SELECTOR, "form")
    login = form.find_element(By.NAME, "login")
    password = form.find_element(By.NAME, "password")
    login.send_keys(user)
    password.send_keys(passwd)
    submit = form.find_element(By.CSS_SELECTOR, "button")
    submit.click()
    _wait_for_url_change(driver, timeout, old_url=idp_url)
    _wait_for_page_load(driver, timeout)
    approval_url = driver.current_url
    approve = driver.find_element(By.CSS_SELECTOR, "button.theme-btn--success")
    approve.click()
    _wait_for_url_change(driver, timeout, old_url=approval_url)
    _wait_for_page_load(driver, timeout)


def _gitlab_group_id(client, path):
    """Return Group ID or -1 if it does not exist for given path."""
    try:
        group = client.groups.get(urllib.parse.quote_plus(path))
    except GitlabGetError as e:
        if "404 Group Not Found" not in e.error_message:
            raise e
        return -1
    return group.id


def _gitlab_user_by_name(client, name):
    """Return array of Users found when searched by name."""
    username = name.split("@", 1)[0]
    return client.users.list(username=username)


def _gitlab_group_member_info(group, uid):
    """Return Group Membership information for given User ID or None if not found."""
    try:
        member = group.members.get(uid)
    except GitlabGetError as e:
        if "404 Not found" not in e.error_message:
            raise e
        return None
    return member


def test_native_login(selenium, service_url):
    timeout = 20
    selenium.get(service_url)
    login_url = selenium.current_url
    form = selenium.find_element(By.CSS_SELECTOR, "form")
    login = form.find_element(By.NAME, "user[login]")
    password = form.find_element(By.NAME, "user[password]")
    login.send_keys("root")
    password.send_keys("nots3kR3t")
    form.submit()
    _wait_for_url_change(selenium, timeout, old_url=login_url)
    _wait_for_page_load(selenium, timeout)
    assert selenium.current_url == service_url
    assert selenium.title == "Projects · GitLab"


def test_oidc_login(selenium, service_url):
    _oidc_login(selenium, 20, service_url, "alice.smith@example.com", "nots3kr3t")
    # GitLab seems to randomly (??) redirect to the dashboard page instead of
    # the home page.
    assert selenium.current_url in (
        service_url,
        urllib.parse.urljoin(service_url, "dashboard/projects"),
    )
    assert selenium.title == "Projects · GitLab"


def test_oidc_existing_group_add_member(selenium, service_url, gl):
    # ensure that charlie, which is member of the "maintainers" and
    # "otherproject" groups, gets added to the already-existing "maintainers"
    # group, and that the mediator ignores groups that do not exist on GitLab
    user, email, passwd = "charlie", "charlie.williams@example.com", "0b501e7ed"
    existing_group = "mediated/maintainers"
    missing_group = "mediated/otherproject"
    timeout, retries = 20, 10
    interval = timeout / retries

    g = gl.groups.get(existing_group)
    with pytest.raises(GitlabGetError):
        # ensure that `missing_group` does not actually exist on GitLab
        gl.groups.get(missing_group)

    _oidc_login(selenium, timeout, service_url, email, passwd)

    # user added as a member? (asynchronous; might need retrying)
    users = _gitlab_user_by_name(gl, user)
    assert len(users) == 1

    member = None
    while member is None and retries:
        member = _gitlab_group_member_info(g, users[0].id)
        retries -= 1
        time.sleep(interval)
    assert member is not None
    assert member.state == "active"
