// Copyright (C) 2021,      Paweł Wieczorek
// Copyright (C) 2021-2022, Andrej Shadura
// Copyright (C) 2021—2022, Collabora Limited
//
// SPDX-License-Identifier: Apache-2.0

package claims_test

import (
	"github.com/coreos/go-oidc/v3/oidc"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	. "gitlab.apertis.org/infrastructure/authz-mediator/claims"
)

var _ = Describe("Claims", func() {
	Describe("Extract", func() {
		var (
			c   *Claims
			err error
		)

		BeforeEach(func() {
			c, err = Extract(&oidc.IDToken{})
		})

		It("should fail and return no data for malformed ID token", func() {
			Expect(err).To(HaveOccurred())
			Expect(err.Error()).To(ContainSubstring("error decoding ID token claims:"))
			Expect(c).To(BeNil())
		})
	})

	Describe("Map and Filter", func() {
		var (
			c Claims
		)

		BeforeEach(func() {
			c = Claims{
				Name:     "User Name",
				Username: "username",
				Email:    "e@mail.org",
				Groups: []string{
					"group1", "group2", "group3", "bad_group", "mapping_required",
				},
			}
		})

		It("should remove denied groups", func() {
			g := FilterGroups(c.Groups, []string{"*"}, []string{"bad_*"})
			Expect(g).To(Equal([]string{
				"group1", "group2", "group3", "mapping_required",
			}))
		})

		It("should keep allowed groups", func() {
			g := FilterGroups(c.Groups, []string{"group*"}, []string{})
			Expect(g).To(Equal([]string{
				"group1", "group2", "group3",
			}))
		})

		It("should keep allowed groups but not denied groups", func() {
			g := FilterGroups(c.Groups, []string{"*group*"}, []string{"mapping_*"})
			Expect(g).To(Equal([]string{
				"group1", "group2", "group3", "bad_group",
			}))
		})

		It("should keep allowed groups but not denied groups, mapping should happen", func() {
			g := FilterGroups(c.Groups, []string{"*group*", "mapping_*"}, []string{"bad_*"})
			g = MapGroups(g, map[string]string{
				"mapping_required": "group4",
			})
			Expect(g).To(Equal([]string{
				"group1", "group2", "group3", "group4",
			}))
		})

	})
	Describe("VerifyGroups", func() {
		var (
			c Claims
		)

		BeforeEach(func() {
			c = Claims{
				Name:     "User Name",
				Username: "username",
				Email:    "e@mail.org",
				Groups: []string{
					"group1", "group2", "group3",
				},
			}
		})

		It("should correctly verify groups", func() {
			var r bool
			r = VerifyGroups(c.Groups, []string{"group1"})
			Expect(r).To(BeTrue())

			r = VerifyGroups(c.Groups, []string{"group1", "group2"})
			Expect(r).To(BeTrue())

			r = VerifyGroups(c.Groups, []string{"group3", "group2"})
			Expect(r).To(BeTrue())

			r = VerifyGroups(c.Groups, []string{"group[23]"})
			Expect(r).To(BeTrue())

			r = VerifyGroups(c.Groups, []string{"group*"})
			Expect(r).To(BeTrue())

			r = VerifyGroups(c.Groups, []string{"group3", "group4"})
			Expect(r).To(BeFalse())

			r = VerifyGroups(c.Groups, []string{"group4", "group1"})
			Expect(r).To(BeFalse())

			r = VerifyGroups(c.Groups, []string{"group[45]"})
			Expect(r).To(BeFalse())
		})
	})

})
