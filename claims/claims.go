// Copyright (C) 2021,      Paweł Wieczorek
// Copyright (C) 2021-2022, Andrej Shadura
// Copyright (C) 2021—2022, Collabora Limited
//
// SPDX-License-Identifier: Apache-2.0

package claims

import (
	"fmt"
	"path"
	"strings"

	"github.com/coreos/go-oidc/v3/oidc"
)

type Claims struct {
	Name     string   `json:"name"`
	Username string   `json:"preferred_username"`
	Email    string   `json:"email"`
	Groups   []string `json:"groups"`
}

func Extract(token *oidc.IDToken) (*Claims, error) {
	var claims Claims
	if err := token.Claims(&claims); err != nil {
		return nil, fmt.Errorf("error decoding ID token claims: %w", err)
	}
	return &claims, nil
}

// FilterGroups removes denied groups and keeps allowed groups.
// Removals take precedence. If both lists are empty, all groups are removed.
func FilterGroups(groups []string, allowed []string, denied []string) []string {
	var new_groups []string
group_loop:
	for _, group := range groups {
		for _, pattern := range denied {
			if match, _ := path.Match(pattern, group); match {
				continue group_loop
			}
		}
		for _, pattern := range allowed {
			if match, _ := path.Match(pattern, group); match {
				new_groups = append(new_groups, group)
			}
		}
	}
	return new_groups
}

// MapGroups converts upstream group names to local group names.
// Unknown groups are preserved in the claim.
func MapGroups(groups []string, group_map map[string]string) []string {
	var new_groups []string
	for _, group := range groups {
		mapped, ok := group_map[group]
		if ok {
			new_groups = append(new_groups, mapped)
		} else {
			new_groups = append(new_groups, group)
		}
	}
	return new_groups
}

// LowerGroups converts all group names to lowercase
func LowerGroups(groups []string) []string {
	lowered := make([]string, len(groups))
	for i, group := range groups {
		lowered[i] = strings.ToLower(group)
	}
	return lowered
}

func VerifyGroups(groups []string, required []string) bool {
req_loop:
	for _, req_group := range required {
		for _, group := range groups {
			if match, _ := path.Match(req_group, group); match {
				continue req_loop
			}
		}
		return false
	}
	return true
}

func UsernameLocalPart(username string) string {
	localPart := strings.Split(username, "@")[0]
	return localPart
}
