package claims_test

import (
	"testing"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

func TestClaims(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Claims Suite")
}
