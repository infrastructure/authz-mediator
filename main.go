// Copyright (C) 2021, Paweł Wieczorek
// Copyright (C) 2021, Collabora Limited
//
// SPDX-License-Identifier: Apache-2.0

package main

import (
	"context"
	"flag"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"github.com/apex/log"
	"github.com/coreos/go-oidc/v3/oidc"

	"gitlab.apertis.org/infrastructure/authz-mediator/config"
	"gitlab.apertis.org/infrastructure/authz-mediator/endpoints"
	"gitlab.apertis.org/infrastructure/authz-mediator/mediator"
)

var (
	configFile  string
	groupsScope bool
	backends    map[string]string = map[string]string{
		mediator.GitLabBackend: "GITLAB_SECRET",
		mediator.OBSBackend:    "OBS_SECRET",
		mediator.LAVABackend:   "LAVA_SECRET",
	}
)

func setFlags() {
	flag.StringVar(&configFile, "config", "", "config file path")
	flag.BoolVar(&groupsScope, "groups", false, "append groups scope to token requests")
}

func main() {
	ctx, stop := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)
	defer stop()

	setupLog()

	setFlags()
	flag.Parse()

	c := &config.Config{}
	if configFile != "" {
		var err error
		if c, err = config.GetConfig(configFile); err != nil {
			log.Fatalf("failed to get config from file %s: %v", configFile, err)
		}
	}
	c.ApplyEnv()
	c.ApplyDefaults()
	if groupsScope {
		c.GroupsScope = true
	}
	log.Infof("Appending groups scope to the relayed requests: %t", groupsScope)

	backendSecret := ""
	if c.Backend != mediator.NoneBackend {
		backendSecretEnv, ok := backends[c.Backend]
		if !ok {
			log.Fatalf("requested backend is not supported: %s", c.Backend)
		}
		backendSecret, ok = os.LookupEnv(backendSecretEnv)
		if !ok {
			log.Fatalf("no backend secret provided")
		}
	}
	mediatorSecret, ok := os.LookupEnv("AUTHZ_MEDIATOR_SECRET")
	if !ok {
		log.Fatalf("no OIDC Client secret provided")
	}

	httpClient := http.DefaultClient
	clientCtx := oidc.ClientContext(ctx, httpClient)
	provider, err := oidc.NewProvider(clientCtx, c.Provider)
	if err != nil {
		log.Fatalf("failed to query provider %q: %v", c.Provider, err)
		return
	}
	ep, err := endpoints.NewEndpoints(c.Provider, httpClient)
	if err != nil {
		log.Fatalf("failed to query provider %q for endpoints: %v", c.Provider, err)
		return
	}

	verifier := provider.Verifier(&oidc.Config{ClientID: c.ID})
	med, err := mediator.NewMediator(mediatorSecret, backendSecret, httpClient, verifier, ep, c)
	if err != nil {
		log.Fatalf("failed to create mediator: %v", err)
	}

	mediatorUrl, err := url.Parse(c.Mediator)
	if err != nil {
		log.Fatalf("failed to parse URL: %s", err)
	}

	host := fmt.Sprintf(":%d", c.Port)

	handleFunc(mediatorUrl.Path+mediator.AuthRoute, med.AuthHandler)
	handleFunc(mediatorUrl.Path+mediator.TokenRoute, med.TokenHandler)
	handleFunc(mediatorUrl.Path+mediator.PubKeysRoute, med.PubKeysHandler)
	handleFunc(mediatorUrl.Path+mediator.UserInfoRoute, med.UserInfoHandler)
	handleFunc(mediatorUrl.Path+mediator.CallbackRoute, med.CallbackHandler)
	handleFunc(mediatorUrl.Path+endpoints.MetadataPath, med.MetadataHandler)

	srv := &http.Server{
		Addr: host,
	}

	log.Infof("Listening on %s%s", host, mediatorUrl.Path)
	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.WithError(err).Fatal("listen")
		}
	}()

	syncing := sync.Mutex{}

	ontick := func() {
		if c.EnforceGroups {
			if syncing.TryLock() {
				log.Infof("Performing group membership enforcement")
				med.EnforceGroups()
				syncing.Unlock()
			} else {
				log.Infof("Another enforcement in progress, skipping")
			}
		}
	}

	if c.EnforceGroups {
		log.Infof("Will enforce groups each %v, ignoring users %v", c.EnforceGroupsInterval, c.EnforceGroupsIgnoreUsers)
	}
	ticker := time.NewTicker(c.EnforceGroupsInterval)
	defer ticker.Stop()

	go ontick()

	for {
		select {
		case <-ticker.C:
			go ontick()
		case <-ctx.Done():
			log.Infof("Shutting down")
			if err := srv.Shutdown(context.Background()); err != nil {
				log.WithError(err).Fatal("shutdown")
			}
			stop()
			return
		}
	}
}
